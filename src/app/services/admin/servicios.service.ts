import { Injectable } from '@angular/core';
import { HttpClient  } from '@angular/common/http';
import { URL } from '../../../app/config/conf';

@Injectable({
  providedIn: 'root'
})
export class ServiciosService {

  public url = URL;


  constructor( private _http: HttpClient ) { }
  
  public getSells( bodyFecha ){

    // console.log( bodyFecha )
    const url = this.url + "/ver/ventas/servicios"
    return this._http.post( url , bodyFecha);

  }

  public getSellsForDate( body){
    const url = this.url + "/ver/ventas/fecha/sede";
    return this._http.post( url, body );
  }

  public getSellsDetailByUser( idPaciente, ){
    const url = this.url +  `/ventas/por/usuario/${idPaciente}`;
    // console.log( url );
    return this._http.get( url ); 
  }
  
}
