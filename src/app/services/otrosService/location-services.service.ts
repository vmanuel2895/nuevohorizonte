import { Injectable } from '@angular/core';
import { map } from 'rxjs/operators';
import { HttpClient } from '@angular/common/http';

@Injectable({
  providedIn: 'root'
})
export class LocationServicesService {

  constructor(public http: HttpClient) { }

  getCountries() {
    const URLCountries = "https://restcountries.eu/rest/v2/all";

    return this.http.get(URLCountries)
      .pipe(map((countries) => {
        return countries;
      }))
  }

  getLocalidades(codigo: number) {

    
    const URLCODIGOPOSTAL = "https://api.copomex.com/query/info_cp/";
    // obtenemos las localidades por el Codigo postal
    let tipo = '?type=simplified';
    //TOKEN
    const token = "&token=0a836ca5-2404-4b4f-b52a-08eb5a365e8b";

    let url = `${URLCODIGOPOSTAL}${codigo}${tipo}${token}`;

    return this.http.get(url)
      .pipe(map(res => res));

  }

  getEstado() {

    const url = "https://api.copomex.com/query/get_estados";
    //TOKEN
    const token = "?token=0a836ca5-2404-4b4f-b52a-08eb5a365e8b";

    return this.http.get(url +  token);
  }


  getMunicipios(municipio) {
    const url = "https://api.copomex.com/query/get_municipio_por_estado/" + municipio;
    //TOKEN
    const token = "?token=0a836ca5-2404-4b4f-b52a-08eb5a365e8b";

    return this.http.get(url + token);

  }

}
