import { ComponentFixture, TestBed } from '@angular/core/testing';

import { FichaInfoSocioComponent } from './ficha-info-socio.component';

describe('FichaInfoSocioComponent', () => {
  let component: FichaInfoSocioComponent;
  let fixture: ComponentFixture<FichaInfoSocioComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ FichaInfoSocioComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(FichaInfoSocioComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
