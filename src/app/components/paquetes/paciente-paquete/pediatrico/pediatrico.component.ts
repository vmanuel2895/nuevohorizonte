import { Component, OnInit, Input } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { PaquetesService } from '../../../../services/paquetes/paquetes.service';
import * as moment from 'moment';
import swal from 'sweetalert';
import { PagoServiciosService } from 'src/app/services/pagos/pago-servicios.service';
import Dates from 'src/app/classes/dates/date.class';
import { getDataStorage } from 'src/app/functions/storage.funcion';
import { CEDE } from 'src/app/classes/cedes/cedes.class';
import Tickets from 'src/app/classes/tickets/ticket.class';
import { eliminarStorage } from 'src/app/functions/pacienteIntegrados';
import PacienteStorage from 'src/app/classes/pacientes/pacientesStorage.class';
/* import swal from 'sweetalert' */

@Component({
  selector: 'app-pediatrico',
  templateUrl: './pediatrico.component.html',
  styleUrls: ['./pediatrico.component.css']
})
export class PediatricoComponent implements OnInit {

  @Input() id: String;
  consultas:any = { tipo: '', consulta: '', fecha: '', medico: '', firma: '' }
  item:any ={ nombreEstudio:'', precioCon:0, precioSin:0, _id:''}
  medicos:any[] = []
  paquete:any[] = []
  concepto:any[] = []
  citas:any[] = []
  examenes:any[] = []
  laboratorio:any[] = []
  public egoCount = 0;
  public ego2Count=0;
  public ego3Count = 0;
  public ego4Count=0;
  public contador=0;
  public urgencias = {fecha:"", hora:"" , consulta:"", medico: "", firma:"" };
  public estancias = {fecha:"", hora:"" , consulta:"", medico: "", firma:"" };
   // public paqueteExamenesInto = [];
  public fechaConsulta = moment().format('l');
  public horaIngreso = moment().format('hh:mm');
  public btnAgregaConsultas = false;

  public pacienteInfo={
    nombrePaciente: "",
    apellidoPaterno: "",
    apellidoMaterno: "",
    curp: "",
    edad: 0,
    genero: "",
    id:"",
    callePaciente: "",
    fechaNacimientoPaciente:"",
    estadoPaciente: "",
    paisPaciente: "",
    telefono: "",
    _id:""
  };
  public infoVenta = {  

    paciente:"",
    nombrePaciente:"",
    vendedor:"",
    fecha:"",
    hora:"",
    estudios:[],
    efectivo:false,
    doctorQueSolicito:"",
    transferencia: false,
    tarjetCredito:false,
    tarjetaDebito:false,
    montoEfectivo:0,
    montoTarjteCredito:0,
    montoTarjetaDebito:0,
    montoTranferencia: 0,
    sede:"",
    totalCompra:0,
    prioridad:"",
    compraConMembresia:true

  }

  public pedidosLaboratorios = { 
    estudios:[],
    idPaciente:"",
    fecha:"",
    hora:"",
    medicoQueSolicita:"",
    sede:"",
    prioridad:"Programado",
    estado:""
  }

  public carrito = {
    totalSin: 0,
    totalCon: 0,
    items: []
  };
  
  fecha: string;
  constructor(public _router: ActivatedRoute, public _paquete:PaquetesService, public _route:Router, private _pagoServicios: PagoServiciosService) { }

  ngOnInit(): void {
    this.obtenerPaquete();
    this.obtenerMedicos();
    this.consultas.firma = JSON.parse(localStorage.getItem('usuario')).nombre;
  }

  obtenerPaquete(){
    this._paquete.obtenerPaquete(this.id)
    .subscribe(  (data:any) =>  {
      this.pacienteInfo = data['paquetes'][0]['paciente'];
      this.paquete = data['paquetes']
      this.citas = this.paquete[0].visitas
      this.examenes = this.paquete[0].examenesLab;
      this.verCosnultashechas();
    });
  }

  obtenerMedicos(){
    this._paquete.getMedicos()
    .subscribe( (data) => {
      this.medicos = data['medicos']
    });
  }

  irAUnServicio(  servicio ){
    this.setRouteService( servicio );
  }

  setRouteService(servicio){
    this._route.navigate([ `/serviciosInt/${servicio}`]);
  }

  verCosnultashechas(){
    // esta funcion se encarga de ver cuales de los paquetes ya estan hechas y las colorea
    let cardUrgencias = document.querySelector('#urgencias');
    let badegUrgencias = document.querySelector('#badgeUrgencias');
    // consultas despues del parto
    let cardEstancias = document.querySelector('#estanciasCortas');
    let badgeEstancias = document.querySelector('#badgeEstancias');
    // incia el codifo que valida las consultas 
    this.citas.forEach(element => {
      // console.log( element );
      if( element.consulta == "Cita abierta a Urgencias con Médico General" ){
        // console.log("Se lo lee");
        cardUrgencias.classList.add('border-primary');
        badegUrgencias.innerHTML = "1";
        badegUrgencias.classList.add('badge-primary');
        this.urgencias = element ;
      } 
      if( element.consulta === 'Estancias cortas de hasta 8 Horas en Urgencias'){
        cardEstancias.classList.add('border-primary');
        badgeEstancias.innerHTML = '1';
        badgeEstancias.classList.add('badge-primary');
        this.estancias = element;
      }
    });
    // termian el codigo que valida las consultas 
    this.verEstudios();
  }

  verEstudios(){
    this.egoCount = 0;
    this.ego2Count=0;
    this.ego3Count = 0;
    this.ego4Count=0;
    // badgeQuimica
   //   // quimicaSanguineaCard
    this.examenes.forEach( (examen:consultas) => {
      if( examen.consulta === "1 Grupo Sanguineo y Facto RH"  ){
        this.egoCount += 1;
        let cardSanguineo = document.querySelector('#SanguineoCard');
        let badgeSanguineo = document.querySelector("#badgeSanguineo");
        this.addClassesCss( cardSanguineo, badgeSanguineo );
      }
      if( examen.consulta === "1 Biometria Hematica Completa"  ){
        this.ego2Count += 1;
        let cardHematica = document.querySelector('#biometriaHematica');
        let badgeHematica = document.querySelector("#badgeHematica");
        this.addClassesCss( cardHematica, badgeHematica );
      }
      if(examen.consulta === "2 Examen General de Orina"){
        this.ego3Count += 1;
        let cardGO = document.querySelector('#GOCard');
        let badgeGO = document.querySelector('#badgeGO');
        this.addClassesCss( cardGO, badgeGO );
      }
      if( examen.consulta === "1 Examen coprológico"){
        this.ego4Count += 1;
        let grupoCoprologico = document.querySelector("#coprologicoCard");
        let badgeCoprologico = document.querySelector("#badgeCopologico");
        this.addClassesCss(grupoCoprologico, badgeCoprologico)
      }
    });
   //   //  termina el codigo que valida los examenes 
  }

  showMessage(examen: string){
    this.examenes.forEach(  (estudio:consultas, index) => {
      // console.log( estudio.consulta  === examen  );
      // console.log( estudio  );
        if( estudio.consulta ===  examen  ){
          // console.log(estudio.consulta, index);
          swal( `Tipo:  ${this.examenes[index].consulta}\n  Fecha: ${this.examenes[index].fecha} \n Hora: ${this.examenes[index].hora}\n   Médico ${this.examenes[index].medico}\n  Atendió: ${this.examenes[index].firma}` );
        }
    });
  }

  addClassesCss( card: Element, badge:Element ){
    card.classList.add('border-primary');
    if( badge.id == "badgeSanguineo"  ){
      badge.innerHTML = this.egoCount.toString(); 
    }
    if(badge.id == 'badgeHematica'){
      badge.innerHTML = this.ego2Count.toString();
    }
    if(badge.id == 'badgeGO'){
      badge.innerHTML = this.ego3Count.toString();
    }
    if(badge.id == 'badgeCopologico'){
      badge.innerHTML = this.ego4Count.toString();
    }
    badge.classList.add('badge-primary');
  }

  mostrarUrgencias(){
    console.log( this.urgencias );
    swal( `Tipo:  ${this.urgencias.consulta}\n  Fecha: ${this.urgencias.fecha} \n Hora: ${this.urgencias.hora}\n   Médico ${this.urgencias.medico}\n  Atendió: ${this.urgencias.firma}` );
  }

  mostrarEstancias(){
    swal( `Tipo:  ${this.estancias.consulta}\n  Fecha: ${this.estancias.fecha} \n Hora: ${this.estancias.hora}\n   Médico ${this.estancias.medico}\n  Atendió: ${this.estancias.firma}` );
  }

  cambiarVisita( selectCon ){
    let nombreConsultaSelect = selectCon.value ;
    if(this.consultas.tipo  === 'examenesLab' ){
      // let log = console.log;
      // checar si nos se han consumido los laboratorios
      this.examenes.forEach((examen:consultas) =>  {
          if( nombreConsultaSelect == examen.consulta && examen.consulta == nombreConsultaSelect ){
            swal("Este elemento ya ha sido usado",{icon:"error"})
            /* alert('Este elemento ya ha sido usado'); */
            // console.log("Biometria hematica");
            this.btnAgregaConsultas = true;
          }
      });
    }
    
    this.citas.forEach( element => {
      if( nombreConsultaSelect ===  element.consulta && element.consulta === "Cita abierta a Urgencias con Médico General"  ){
        swal("Este elemento ya ha sido usado",{icon:"error"})
        /* alert('Este elemento ya ha sido usado'); */
        this.btnAgregaConsultas = true;
      }else if(nombreConsultaSelect ===  element.consulta && element.consulta === "Estancias cortas de hasta 8 Horas en Urgencias"){
        this.btnAgregaConsultas = true;
        swal("Este elemento ya ha sido usado",{icon:"error"})
      }
      this.btnAgregaConsultas = false;
      if(  nombreConsultaSelect  != "Cita abierta a Urgencias con Médico General" && nombreConsultaSelect  != "Estancias cortas de hasta 8 Horas en Urgencias" && nombreConsultaSelect != "4 Estudios de laboratorio" && nombreConsultaSelect != "1 Grupo Sanguineo y Facto RH"  && nombreConsultaSelect != "1 Biometria Hematica Completa" && nombreConsultaSelect != "2 Examen General de Orina" && nombreConsultaSelect != "1 Examen coprológico"){
        this.btnAgregaConsultas = false;        
      }
      if( this.egoCount == 4  ){
        this.btnAgregaConsultas = false;
      }
      if( this.ego2Count == 2  ){
        this.btnAgregaConsultas = false;
      }
    }) 
  }

  //insercion
  seleccion($event, value){
    switch (value) {
      case 'visitas':
        this.concepto=[]
        for(let item of this.paquete ){
          for(let item2 of item.paquete.CitasIncluidas){
            this.concepto.push(item2)
          }
          console.log(this.concepto)
        }  
        this.consultas.consulta = ''
      break;

      case 'examenesLab':
        this.concepto=[]
        for(let item of this.paquete ){
          for(let item2 of item.paquete.examenesLaboratorio){
            this.concepto.push(item2)
          }
          console.log(this.concepto)
        }  
        this.consultas.consulta = ''
      break;

      default:
        break;
    }
  } 

  agregarConsulta(){
    this.consultas.fecha = new Date()
    this.consultas.hora = this.horaIngreso;
    this.consultas.firma = JSON.parse(localStorage.getItem('usuario')).nombre;

    if(this.consultas.tipo == '' || this.consultas.consulta == '' || this.consultas.medico == '' || this.consultas.firma == ''){
      swal('Error!', 'Porfavor ingrese los datos que le piden', 'error')
    }else{
      if(this.consultas.tipo == 'visitas'){
        const encontrar = this.citas.findIndex(element => element.consulta == "1 Certificado Médico Escolar");
        if(encontrar => 0){
          swal('Error!','Se terminaron las consultas de tu paquete.', 'error')
          this.consultas = { tipo: '', consulta: '', fecha: '', medico: '', firma:'' }
        }else{
          this._paquete.agregarConsulta(this.consultas,this.consultas.tipo,this.id).subscribe(
          (data)=>{
            this.mostrarConsultas();
            this._pagoServicios.agregarPedido( this.infoVenta )
            .subscribe( (data) => {
              // console.log( data );
                if(  data['ok'] ){
                
                  this.generarTicket(data['folio']);
                  // se crea la tabla de las ventas 
                  swal('Consulta Agregada', 'Puede ver las visitas en la tabla', 'success')
  
                  // ESTA INFORMCAIÓN SE ENVIA A LOS DIFERENTES SERVICIOS 
                  // seteamos las fechas 
                  eliminarStorage();
                    
                    
                  const eliminarPaciente = new PacienteStorage();
                  eliminarPaciente.removePacienteStorage();
                  this.consultas = { tipo: '', consulta: '', fecha: '', medico: '', firma:'' } 
                  this._route.navigateByUrl('consulta');   
                }
            });
          })
        }        
      }else{
        let val = true;
        val = this.validaciones();
        if(val){
          this._paquete.agregarConsulta(this.consultas,this.consultas.tipo,this.id).subscribe(
            (data)=>{
              this.setDatesVenta(this.consultas.medico);          
              this.pagar_consulta(this.consultas.consulta, this.consultas.medico);
              this.consultas = { tipo: '', consulta: '', fecha: '', medico: '', firma:'' }
            })
        }else{
          swal('Error!','Se terminaron los laboratorios de tu paquete.', 'error')
          this.consultas = { tipo: '', consulta: '', fecha: '', medico: '', firma:'' }
        }
      }
    }
  }

  validaciones(){
    switch(this.consultas.consulta){      
      case '1 Grupo Sanguineo y Facto RH':
        for (const iterator of this.examenes) {
          if(iterator.consulta == '1 Grupo Sanguineo y Facto RH'){
            this.contador= this.contador + 1
          }
        }
        if(this.contador < 1){
          return true;
        }else{
          return false;
        }
        break;
      case '1 Biometria Hematica Completa':
          for (const iterator of this.examenes) {
            if(iterator.consulta == '1 Biometria Hematica Completa'){
              this.contador= this.contador + 1
            }
          }
          console.log('asghasgdh');
          
          if(this.contador < 1){
            return true;
          }else{
            return false;
          }
        break;
      case '2 Examen General de Orina':
          for (const iterator of this.examenes) {
            if(iterator.consulta == '2 Examen General de Orina'){
              this.contador= this.contador + 1
            }
          }
          console.log(this.contador);
          
          if(this.contador < 2){
            return true;
          }else{
            return false;
          }
        break;
      case '1 Examen coprológico':
          for (const iterator of this.examenes) {
            if(iterator.consulta == '1 Examen coprológico'){
              this.contador= this.contador + 1
            }
          }
          if(this.contador < 1){
            return true;
          }else{
            return false;
          }
        break;
    }
  }

  //metodo 
  setDatesVenta(medico){
    const item = {
      nombreEstudio: "CONSULTA DE MEDICO GENERAL",
      precioCon:0,
      precioSin:0,
      _id:'5fd3ebca08ccbb0017712f0d'
    }
    const dates = new Dates();
    //this.infoVenta.totalCompra = this.carrito.totalSin;
    this.fecha = moment().format('l');    
    this.infoVenta.hora = moment().format('LT');
    this.infoVenta.vendedor = getDataStorage()._id;
    if(this.pacienteInfo.id == undefined){
      this.infoVenta.paciente = this.pacienteInfo._id;
    }else{
      this.infoVenta.paciente = this.pacienteInfo.id;
    }
    this.infoVenta.sede = CEDE;
    this.infoVenta.prioridad = "Programado"
    this.infoVenta.fecha = dates.getDate();
    this.infoVenta.doctorQueSolicito = medico;
    this.carrito.items.push(item);
    this.infoVenta.estudios= this.carrito.items
    this.infoVenta.totalCompra = 0;    
  }

  pagar_consulta(nombre,medico){
    const dates = new Dates();
    switch (nombre) {
        case "1 Grupo Sanguineo y Facto RH":
          this.carrito={
            totalSin: 0,
            totalCon: 0,
            items: []
          };
          this.item.nombreEstudio = "GRUPO Y FACTOR RH";
          this.item._id = '5fd292b31cb0ea0017f57c9e';
          this.fecha = moment().format('l');    
          this.infoVenta.hora = moment().format('LT');
          this.infoVenta.vendedor = getDataStorage()._id;
          if(this.pacienteInfo.id == undefined){
            this.infoVenta.paciente = this.pacienteInfo._id;
          }else{
            this.infoVenta.paciente = this.pacienteInfo.id;
          }
          this.infoVenta.nombrePaciente = this.pacienteInfo.nombrePaciente+ ' ' + this.pacienteInfo.apellidoPaterno+ ' ' + this.pacienteInfo.apellidoMaterno
          this.infoVenta.sede = CEDE;
          this.infoVenta.prioridad = "Programado"
          this.infoVenta.fecha = dates.getDate();
          this.infoVenta.doctorQueSolicito = medico;
          /* this.carrito.items.push(this.item); */
          this.infoVenta.estudios= this.carrito.items
          this.infoVenta.totalCompra = 0;
          const item = {
            nombreEstudio: "GRUPO Y FACTOR RH",
            precioCon:0,
            precioSin:0,
            idEstudio:'5fd292b31cb0ea0017f57c9e'
          }
          this.carrito.items.push(item);
          this._pagoServicios.agregarPedido( this.infoVenta )
          .subscribe( (data) => {
            // console.log( data );
            if(  data['ok'] ){
              
              this.generarTicket(data['folio']);
                  this.setDatesPedidos();
                  this.pedidosLaboratorios.estudios.push(item);
                  console.log(this.pedidosLaboratorios);
                  
                  this._pagoServicios.pedidosLaboratorio( this.pedidosLaboratorios ).subscribe( data => console.log( data ));
                // se crea la tabla de las ventas 
                swal('Consulta Agregada', 'Puede ver las visitas en la tabla', 'success')

                // ESTA INFORMCAIÓN SE ENVIA A LOS DIFERENTES SERVICIOS 
                // seteamos las fechas 
                  eliminarStorage();
                  
                  
                  const eliminarPaciente = new PacienteStorage();
                  eliminarPaciente.removePacienteStorage();  
              }
          });
          this.mostrarConsultas();
          break;
        case "1 Biometria Hematica Completa":
            this.carrito={
              totalSin: 0,
              totalCon: 0,
              items: []
            };
            this.item.nombreEstudio = "BIOMETRIA HEMATICA COMPLETA";
            this.item._id = '5fd284b11cb0ea0017f57bd8';
            this.fecha = moment().format('l');    
            this.infoVenta.hora = moment().format('LT');
            this.infoVenta.vendedor = getDataStorage()._id;
            if(this.pacienteInfo.id == undefined){
              this.infoVenta.paciente = this.pacienteInfo._id;
            }else{
              this.infoVenta.paciente = this.pacienteInfo.id;
            }
            this.infoVenta.nombrePaciente = this.pacienteInfo.nombrePaciente+ ' ' + this.pacienteInfo.apellidoPaterno+ ' ' + this.pacienteInfo.apellidoMaterno
            this.infoVenta.sede = CEDE;
            this.infoVenta.prioridad = "Programado"
            this.infoVenta.fecha = dates.getDate();
            this.infoVenta.doctorQueSolicito = medico;
            /* this.carrito.items.push(this.item); */
            this.infoVenta.estudios= this.carrito.items
            this.infoVenta.totalCompra = 0;
            const item2 = {
              nombreEstudio: "BIOMETRIA HEMATICA COMPLETA",
              precioCon:0,
              precioSin:0,
              idEstudio:'5fd284b11cb0ea0017f57bd8'
            }
            this.carrito.items.push(item2);
            this._pagoServicios.agregarPedido( this.infoVenta )
            .subscribe( (data) => {
              // console.log( data );
              if(  data['ok'] ){
                
                this.generarTicket(data['folio']);
                  // se crea la tabla de las ventas 
                  this.setDatesPedidos();
                  this.pedidosLaboratorios.estudios.push(item2);
                  console.log(this.pedidosLaboratorios);
                  
                  this._pagoServicios.pedidosLaboratorio( this.pedidosLaboratorios ).subscribe( data => console.log( data ));
                  swal('Consulta Agregada', 'Puede ver las visitas en la tabla', 'success')

                  // ESTA INFORMCAIÓN SE ENVIA A LOS DIFERENTES SERVICIOS 
                  // seteamos las fechas 
                    eliminarStorage();
                    
                    
                    const eliminarPaciente = new PacienteStorage();
                    eliminarPaciente.removePacienteStorage();  
                }
            });
            this.mostrarConsultas();
        break;
        case "2 Examen General de Orina":
                this.carrito={
                  totalSin: 0,
                  totalCon: 0,
                  items: []
                };
                this.item.nombreEstudio = "EXAMEN GENERAL DE ORINA ";
                this.item._id = '5fd28c2f1cb0ea0017f57c58';
                this.fecha = moment().format('l');    
                this.infoVenta.hora = moment().format('LT');
                this.infoVenta.vendedor = getDataStorage()._id;
                if(this.pacienteInfo.id == undefined){
                  this.infoVenta.paciente = this.pacienteInfo._id;
                }else{
                  this.infoVenta.paciente = this.pacienteInfo.id;
                }
                this.infoVenta.nombrePaciente = this.pacienteInfo.nombrePaciente+ ' ' + this.pacienteInfo.apellidoPaterno+ ' ' + this.pacienteInfo.apellidoMaterno
                this.infoVenta.sede = CEDE;
                this.infoVenta.prioridad = "Programado"
                this.infoVenta.fecha = dates.getDate();
                this.infoVenta.doctorQueSolicito = medico;
                /* this.carrito.items.push(this.item); */
                this.infoVenta.estudios= this.carrito.items
                this.infoVenta.totalCompra = 0;
                const item3 = {
                  nombreEstudio: "EXAMEN GENERAL DE ORINA ",
                  precioCon:0,
                  precioSin:0,
                  idEstudio:'5fd28c2f1cb0ea0017f57c58'
                }
                this.carrito.items.push(item3);
                this._pagoServicios.agregarPedido( this.infoVenta )
                .subscribe( (data) => {
                  // console.log( data );
                  if(  data['ok'] ){
                    
                    this.generarTicket(data['folio']);
                      // se crea la tabla de las ventas 
                      this.setDatesPedidos();
                      this.pedidosLaboratorios.estudios.push(item3);
                      console.log(this.pedidosLaboratorios);
                      
                      this._pagoServicios.pedidosLaboratorio( this.pedidosLaboratorios ).subscribe( data => console.log( data ));
                      swal('Consulta Agregada', 'Puede ver las visitas en la tabla', 'success')

                      // ESTA INFORMCAIÓN SE ENVIA A LOS DIFERENTES SERVICIOS 
                      // seteamos las fechas 
                        eliminarStorage();
                        
                        
                        const eliminarPaciente = new PacienteStorage();
                        eliminarPaciente.removePacienteStorage();  
                    }
                });
                this.mostrarConsultas();
        break;
        case "1 Examen coprológico":
                this.carrito={
                  totalSin: 0,
                  totalCon: 0,
                  items: []
                };
                this.item.nombreEstudio = "COPROLOGICO (EXAMEN GENERAL DE HECES)";
                this.item._id = '5fd287ed1cb0ea0017f57c0d';
                this.fecha = moment().format('l');    
                this.infoVenta.hora = moment().format('LT');
                this.infoVenta.vendedor = getDataStorage()._id;
                if(this.pacienteInfo.id == undefined){
                  this.infoVenta.paciente = this.pacienteInfo._id;
                }else{
                  this.infoVenta.paciente = this.pacienteInfo.id;
                }
                this.infoVenta.nombrePaciente = this.pacienteInfo.nombrePaciente+ ' ' + this.pacienteInfo.apellidoPaterno+ ' ' + this.pacienteInfo.apellidoMaterno
                this.infoVenta.sede = CEDE;
                this.infoVenta.prioridad = "Programado"
                this.infoVenta.fecha = dates.getDate();
                this.infoVenta.doctorQueSolicito = medico;
                /* this.carrito.items.push(this.item); */
                this.infoVenta.estudios= this.carrito.items
                this.infoVenta.totalCompra = 0;
                const item4 = {
                  nombreEstudio: "COPROLOGICO (EXAMEN GENERAL DE HECES)",
                  precioCon:0,
                  precioSin:0,
                  idEstudio:'5fd287ed1cb0ea0017f57c0d'
                }
                this.carrito.items.push(item4);
                this._pagoServicios.agregarPedido( this.infoVenta )
                .subscribe( (data) => {
                  // console.log( data );
                  if(  data['ok'] ){
                    
                    this.generarTicket(data['folio']);
                      // se crea la tabla de las ventas 
                      this.setDatesPedidos();
                      this.pedidosLaboratorios.estudios.push(item4);
                      console.log(this.pedidosLaboratorios);
                      
                      this._pagoServicios.pedidosLaboratorio( this.pedidosLaboratorios ).subscribe( data => console.log( data ));
                      swal('Consulta Agregada', 'Puede ver las visitas en la tabla', 'success')

                      // ESTA INFORMCAIÓN SE ENVIA A LOS DIFERENTES SERVICIOS 
                      // seteamos las fechas 
                        eliminarStorage();
                        
                        
                        const eliminarPaciente = new PacienteStorage();
                        eliminarPaciente.removePacienteStorage();  
                    }
                });
                this.mostrarConsultas();
        break;
      default:
        break;
    }
  }

  setDatesPedidos (){
    // this.pedidosLaboratorios.fecha = moment().format('l');
    const datesPedidoLab = new Dates();
    // configuracion de los pedidos de laboratorio
    this.pedidosLaboratorios.fecha = datesPedidoLab.getDate();
    this.pedidosLaboratorios.hora = moment().format('LT');
    this.pedidosLaboratorios.medicoQueSolicita = this.infoVenta.doctorQueSolicito;
    this.pedidosLaboratorios.sede = CEDE;
    this.pedidosLaboratorios.idPaciente = this.infoVenta.paciente;
  }

  mostrarConsultas(){
    this.obtenerPaquete();
  }
  generarTicket(folio){

    const tickets = new Tickets();
    tickets.printTicketSinMembresia( this.pacienteInfo, this.carrito ,  this.infoVenta, folio );
  
  }
  mostrarDatos(consulta, medico, servicio){
    if(servicio == '1') swal('Citas incluidas\n','Tipo de consulta:'+consulta+'\nMedico: '+medico,'')
    if(servicio == '2') swal('Examenes de Laboratorio\n','Tipo de laboratorio:'+consulta+'\nMedico: '+medico,'')
  }
}

class consultas  {

  tipo:  string;
  consulta:  string;
  fecha:  string;
  firma:  string;
  hora:  string;
  medico:  string;

}
