import { Component, OnInit, Input } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { PaquetesService } from '../../../../services/paquetes/paquetes.service';
import * as moment from 'moment';
import swal from 'sweetalert';
import { PagoServiciosService } from 'src/app/services/pagos/pago-servicios.service';
import Dates from 'src/app/classes/dates/date.class';
import { getDataStorage } from 'src/app/functions/storage.funcion';
import { CEDE } from 'src/app/classes/cedes/cedes.class';
import PacienteStorage from 'src/app/classes/pacientes/pacientesStorage.class';
import { eliminarStorage } from 'src/app/functions/pacienteIntegrados';
import Tickets from 'src/app/classes/tickets/ticket.class';

@Component({
  selector: 'app-vida-plena',
  templateUrl: './vida-plena.component.html',
  styleUrls: ['./vida-plena.component.css']
})
export class VidaPlenaComponent implements OnInit {
  
  @Input() id: String;
  consultas:any = { tipo: '', consulta: '', fecha: '', medico: '', firma: '' }
  item:any ={ nombreEstudio:'', precioCon:0, precioSin:0, _id:''}
  concepto:any[] = []
  medicos:any[] = []
  paquete:any[] = []
  citas:any[] = []
  examenes:any[] = []
  public contador = 0;
  public contadorcitas = 0;
  public egoCount = 0;
  public ego2Count = 0;
  public lab1Count =0;
  public lab2Count =0;
  public lab3Count =0;
  public lab4Count =0;
  public ray1Count =0;
  public tom1Count=0;
  
    public hospitalizacion = {fecha:"", hora:"" , consulta:"", medico: "", firma:"" };
    public consultaDespuesParto = {fecha:"", hora:"" , consulta:"", medico: "", firma:"" };
    // public paqueteExamenesInto = [];
    public fechaConsulta = moment().format('l');
    public horaIngreso = moment().format('hh:mm');
    public btnAgregaConsultas = false;
    public pacienteInfo={
      nombrePaciente: "",
      apellidoPaterno: "",
      apellidoMaterno: "",
      curp: "",
      edad: 0,
      genero: "",
      id:"",
      callePaciente: "",
      fechaNacimientoPaciente:"",
      estadoPaciente: "",
      paisPaciente: "",
      telefono: "",
      _id:""
    };
    public infoVenta = {  

      paciente:"",
      nombrePaciente:"",
      vendedor:"",
      fecha:"",
      hora:"",
      estudios:[],
      efectivo:false,
      doctorQueSolicito:"",
      transferencia: false,
      tarjetCredito:false,
      tarjetaDebito:false,
      montoEfectivo:0,
      montoTarjteCredito:0,
      montoTarjetaDebito:0,
      montoTranferencia: 0,
      sede:"",
      totalCompra:0,
      prioridad:"",
      compraConMembresia:true
  
    }
    public pedidosLaboratorios = { 
      estudios:[],
      idPaciente:"",
      fecha:"",
      hora:"",
      medicoQueSolicita:"",
      sede:"",
      prioridad:"Programado",
      estado:""
    }

    public pedidosUltrasonido = {
      idPaciente:"", 
      estudios:[],
      fecha:"",
      hora:"",
      medicoQueSolicita:"",
      sede:"",
      prioridad:"Programado"
    }

    public pedidosRayox = {
      idPaciente:"", 
      estudios:[],
      fecha:"",
      hora:"",
      medicoQueSolicita:"",
      sede:"",
      prioridad:"Programado"
    }

    public carrito = {
      totalSin: 0,
      totalCon: 0,
      items: []
    };
  fecha: string;
  constructor(public _router: ActivatedRoute, public _paquete:PaquetesService, public _route:Router,private _pagoServicios: PagoServiciosService) { }

  ngOnInit(): void {
    this.obtenerMedicos();
    this.obtenerPaquete();
    this.consultas.firma = JSON.parse(localStorage.getItem('usuario')).nombre;
  }

  obtenerPaquete(){
    this._paquete.obtenerPaquete(this.id)
    .subscribe(  (data:any) =>  {
      console.log(data);
      
      this.pacienteInfo = data['paquetes'][0]['paciente'];
      this.paquete = data['paquetes']
      this.citas = this.paquete[0].visitas
      this.examenes = this.paquete[0].examenesLab;
      this.verCosnultashechas();
    });
  }

  obtenerMedicos(){
    this._paquete.getMedicos()
    .subscribe( (data) => {
      this.medicos = data['medicos']
    });
  }

  irAUnServicio(  servicio ){
    this.setRouteService( servicio );
  }

  setRouteService(servicio){
    this._route.navigate([ `/serviciosInt/${servicio}`]);
  }

  verCosnultashechas(){
    this.egoCount = 0;
    this.ego2Count= 0;
    this.citas.forEach(element => {
      // console.log( element );
      if( element.consulta == "8 Consultas programadas con Especialista MEDICINA INTERNA" ){
        // console.log("Se lo lee");
        this.egoCount += 1;
        let cardCPEMI = document.querySelector('#cardCPEMI');
        let badgeCPEMI = document.querySelector('#badgeCPEMI');
        this.addClassesCss( cardCPEMI, badgeCPEMI);
        //this.hospitalizacion = element ;
      } 
      if( element.consulta === 'Cita abierta a urgencias desde la contratación del servicio por un año con Medicina General.'){
        this.ego2Count += 1;
        let cardCA = document.querySelector('#cardCA');
        let badgeCA = document.querySelector('#badgeCA');
        this.addClassesCss( cardCA, badgeCA);
        //this.consultaDespuesParto = element;
      }
    });
    // termian el codigo que valida las consultas 
    this.verEstudios();
  }

  verEstudios(){
    this.lab1Count =0;
    this.lab2Count =0;
    this.lab3Count =0;
    this.lab4Count =0;
    this.ray1Count =0;
    this.tom1Count=0;
    // badgeQuimica
   //   // quimicaSanguineaCard
    this.examenes.forEach( (examen:consultas) => {
      if( examen.consulta === "1 Biometría Hemática Completa " ){
        this.lab1Count += 1;
        let cardBHC = document.querySelector('#BHCCard');
        let badgeBHC = document.querySelector('#badegeBHC');
        this.addClassesCss( cardBHC, badgeBHC);
      }
      if( examen.consulta === "1 QS 6 (Glucosa, Urea, Creatinina, Ácido Úrico, Colesterol y Triglicéridos)"  ){
        this.lab2Count += 1;
        let cardQS = document.querySelector('#QSCard');
        let badgeQS = document.querySelector("#badgeQS");
        this.addClassesCss( cardQS, badgeQS );
      }
      if(examen.consulta === "1 Pruebas de Funcionamiento Hepático"){
        this.lab3Count += 1;
        let cardPFH = document.querySelector('#PFHCard');
        let badgePFH = document.querySelector("#badegePFH");
        this.addClassesCss( cardPFH, badgePFH );
      }
      if(examen.consulta === "1 Exámenes Generales de Orina"){
        this.lab4Count += 1;
        let cardEGO = document.querySelector('#EGOCard');
        let badgeEGO = document.querySelector('#badgeEGO');
        this.addClassesCss( cardEGO, badgeEGO);
      }
      if( examen.consulta === "1 Tele de Tórax."){
        this.ray1Count += 1;
        let cardTT = document.querySelector("#TTCard");
        let badgeTT = document.querySelector("#badgeTT");
        this.addClassesCss(cardTT, badgeTT)
      }
      if( examen.consulta === "1 Electrocardiograma"){
        this.tom1Count += 1
        let cardElectroC = document.querySelector("#ElectroCCard");
        let badgeElectroC = document.querySelector("#badgeElectroC");
        this.addClassesCss(cardElectroC, badgeElectroC)
      }
    });
   //   //  termina el codigo que valida los examenes 
  }


  showMessage(examen: string){
    this.examenes.forEach(  (estudio:consultas, index) => {
      // console.log( estudio.consulta  === examen  );
      // console.log( estudio  );
        if( estudio.consulta ===  examen  ){
          // console.log(estudio.consulta, index);
          swal( `Tipo:  ${this.examenes[index].consulta}\n  Fecha: ${this.examenes[index].fecha} \n Hora: ${this.examenes[index].hora}\n   Médico ${this.examenes[index].medico}\n  Atendió: ${this.examenes[index].firma}` );
        }else{
          this.citas.forEach((cita:consultas, index)=>{
            if(cita.consulta === examen){
              swal( `Tipo:  ${this.citas[index].consulta}\n  Fecha: ${this.citas[index].fecha} \n Hora: ${this.citas[index].hora}\n   Médico ${this.citas[index].medico}\n  Atendió: ${this.citas[index].firma}` );
            }
          })
        }
    });
  }

  addClassesCss( card: Element, badge:Element ){
    card.classList.add('border-primary');

    if( badge.id == "badegeBHC"){
      badge.innerHTML = this.lab1Count.toString(); 
    }
    if(badge.id == 'badgeQS'){
      badge.innerHTML = this.lab2Count.toString();
    }
    //ultra
    if( badge.id == "badegePFH"){
      badge.innerHTML = this.lab3Count.toString(); 
    }
    if(badge.id == 'badgeEGO'){
      badge.innerHTML = this.lab4Count.toString();
    }
    if(badge.id == 'badgeTT'){
      badge.innerHTML = this.ray1Count.toString();
    }
    if(badge.id == 'badgeElectroC'){
      badge.innerHTML = this.tom1Count.toString();
    }
    if(badge.id == 'badgeCPEMI'){
      badge.innerHTML = this.egoCount.toString();
    }
    if(badge.id == 'badgeCA'){
      badge.innerHTML = this.ego2Count.toString();
    }
    badge.classList.add('badge-primary');
  }

  // Nueva programacion de insercion
  seleccion($event, value){
    switch (value) {
      case 'visitas':
        this.concepto=[]
        for(let item of this.paquete ){
          for(let item2 of item.paquete.CitasIncluidas){
            this.concepto.push(item2)
          }
          console.log(this.concepto)
        }  
        this.consultas.consulta = ''
      break;

      case 'laboratorio':
        this.concepto=[]
        for(let item of this.paquete ){
          for(let item2 of item.paquete.examenesLaboratorio){
            this.concepto.push(item2)
          }
          console.log(this.concepto)
        }  
        this.consultas.consulta = ''
      break;
      
      case 'ultrasonido':
        this.concepto=[]
        for(let item of this.paquete ){
          for(let item2 of item.paquete.ultrasonidos){
            this.concepto.push(item2)
          }
          console.log(this.concepto)
        }
        this.consultas.consulta = ''
        
      break;

      case '4':
        this.concepto=[]
        for(let item of this.paquete ){
          for(let item2 of item.paquete.extras){
            this.concepto.push(item2)
          }
          console.log(this.concepto)
        }
        this.consultas.consulta = ''
        
      break;

      default:
        break;
    }
  }

  agregarConsulta(){
    this.consultas.fecha = this.fechaConsulta;
    this.consultas.hora = this.horaIngreso;
    console.log(this.consultas)
    this.consultas.firma = JSON.parse(localStorage.getItem('usuario')).nombre;

    if(this.consultas.tipo == '' || this.consultas.consulta == '' || this.consultas.medico == '' || this.consultas.firma == ''){
      swal('Error!', 'Porfavor ingrese los datos que le piden', 'error')
    }else{
      if(this.consultas.tipo == 'visitas'){
        let val = true;
        val = this.validacioncitas();
        if(val){
          this._paquete.agregarConsulta(this.consultas,this.consultas.tipo,this.id).subscribe(
            (data)=>{
              //swal('Consulta Agregada', 'Puede ver las visitas en la tabla', 'success')
              this.mostrarConsultas();
              this.setDatesVenta(this.consultas.medico);
              this._pagoServicios.agregarPedido( this.infoVenta )
              .subscribe( (data) => {
                // console.log( data );
                if(  data['ok'] ){
                  
                  this.generarTicket(data['folio']);
                    // se crea la tabla de las ventas 
                    swal('Consulta Agregada', 'Puede ver las visitas en la tabla', 'success')
    
                    // ESTA INFORMCAIÓN SE ENVIA A LOS DIFERENTES SERVICIOS 
                    // seteamos las fechas 
                      eliminarStorage();
                      
                      
                      const eliminarPaciente = new PacienteStorage();
                      eliminarPaciente.removePacienteStorage();
                      this.consultas = { tipo: '', consulta: '', fecha: '', medico: '', firma:'' } 
                      this._route.navigateByUrl('consulta');  
                }
              });
            })
        }else{
          swal('Error!','Se terminaron las consultas de tu paquete.', 'error')
          this.consultas = { tipo: '', consulta: '', fecha: '', medico: '', firma:'' }
        }
      }else{
        let val = true;
        val = this.validaciones();
        if(val){
          this.consultas.tipo = 'examenesLab';
          this._paquete.agregarConsulta(this.consultas,this.consultas.tipo,this.id).subscribe(
            (data)=>{
              this.setDatesVenta(this.consultas.medico);          
              this.pagar_consulta(this.consultas.consulta, this.consultas.medico);
              this.consultas = { tipo: '', consulta: '', fecha: '', medico: '', firma:'' }
            })
        }else{
          swal('Error!','Se terminaron los laboratorios de tu paquete.', 'error')
          this.consultas = { tipo: '', consulta: '', fecha: '', medico: '', firma:'' }
        }
      }
    }
  }

  validacioncitas(){
    switch(this.consultas.consulta){      
      case '8 Consultas programadas con Especialista MEDICINA INTERNA':
        for (const iterator of this.citas) {
          if(iterator.consulta == '8 Consultas programadas con Especialista MEDICINA INTERNA'){
            this.contadorcitas= this.contadorcitas + 1
          }
        }
        if(this.contadorcitas < 8){
          return true;
        }else{
          return false;
        }
        break;
      default:
        return true;
      break;
    }
  }

  validaciones(){
    switch(this.consultas.consulta){      
      case '1 Biometría Hemática Completa ':
        for (const iterator of this.examenes) {
          if(iterator.consulta == '1 Biometría Hemática Completa '){
            this.contador= this.contador + 1
          }
        }
        if(this.contador < 1){
          return true;
        }else{
          return false;
        }
        break;
      case '1 QS 6 (Glucosa, Urea, Creatinina, Ácido Úrico, Colesterol y Triglicéridos)':
          for (const iterator of this.examenes) {
            if(iterator.consulta == '1 QS 6 (Glucosa, Urea, Creatinina, Ácido Úrico, Colesterol y Triglicéridos)'){
              this.contador= this.contador + 1
            }
          }
          console.log('asghasgdh');
          
          if(this.contador < 1){
            return true;
          }else{
            return false;
          }
        break;
      case '1 Pruebas de Funcionamiento Hepático':
          for (const iterator of this.examenes) {
            if(iterator.consulta == '1 Pruebas de Funcionamiento Hepático'){
              this.contador= this.contador + 1
            }
          }
          console.log(this.contador);
          
          if(this.contador < 1){
            return true;
          }else{
            return false;
          }
        break;
      case '1 Exámenes Generales de Orina':
          for (const iterator of this.examenes) {
            if(iterator.consulta == '1 Exámenes Generales de Orina'){
              this.contador= this.contador + 1
            }
          }
          if(this.contador < 1){
            return true;
          }else{
            return false;
          }
        break;
      case '1 Tele de Tórax.':
          for (const iterator of this.examenes) {
            if(iterator.consulta == '1 Tele de Tórax.'){
              this.contador= this.contador + 1
            }
          }
          if(this.contador < 1){
            return true;
          }else{
            return false;
          }
        break;
      case '1 Electrocardiograma':
          for (const iterator of this.examenes) {
            if(iterator.consulta == '1 Electrocardiograma'){
              this.contador= this.contador + 1
            }
          }
          if(this.contador < 1){
            return true;
          }else{
            return false;
          }
        break;
    }
  }

  //metodo 
  setDatesVenta(medico){
    if(this.consultas.consulta == '8 Consultas programadas con Especialista MEDICINA INTERNA'){
      const item = {
        nombreEstudio: "CONSULTA MEDICINA INTERNA",
        precioCon:0,
        precioSin:0,
        _id:'5fdd29e5a71eca0017a8e92c'
      }
      this.carrito.items.push(item);
    }else{
      const item = {
        nombreEstudio: "CONSULTA DE MEDICO GENERAL",
        precioCon:0,
        precioSin:0,
        _id:'5fd3ebca08ccbb0017712f0d'
      }
      this.carrito.items.push(item);
    }
    
    const dates = new Dates();
    //this.infoVenta.totalCompra = this.carrito.totalSin;
    this.fecha = moment().format('l');    
    this.infoVenta.hora = moment().format('LT');
    this.infoVenta.vendedor = getDataStorage()._id;
    if(this.pacienteInfo.id == undefined){
      this.infoVenta.paciente = this.pacienteInfo._id;
    }else{
      this.infoVenta.paciente = this.pacienteInfo.id;
    }
    this.infoVenta.sede = CEDE;
    this.infoVenta.prioridad = "Programado"
    this.infoVenta.fecha = dates.getDate();
    this.infoVenta.doctorQueSolicito = medico;
    this.infoVenta.estudios= this.carrito.items
    this.infoVenta.totalCompra = 0;    
  }

  mostrarConsultas(){
    this.obtenerPaquete();
  }

  generarTicket(folio){

    const tickets = new Tickets();
    tickets.printTicketSinMembresia( this.pacienteInfo, this.carrito ,  this.infoVenta, folio);
  
  }
  
  pagar_consulta(nombre,medico){
    this.carrito={
      totalSin: 0,
      totalCon: 0,
      items: []
    };
    const dates = new Dates();
    switch (nombre) {
      case "1 Biometría Hemática Completa ":
        this.item.nombreEstudio = "BIOMETRIA HEMATICA COMPLETA";
            this.item._id = '5fd284b11cb0ea0017f57bd8';
            this.fecha = moment().format('l');    
            this.infoVenta.hora = moment().format('LT');
            this.infoVenta.vendedor = getDataStorage()._id;
            if(this.pacienteInfo.id == undefined){
              this.infoVenta.paciente = this.pacienteInfo._id;
            }else{
              this.infoVenta.paciente = this.pacienteInfo.id;
            }
            this.infoVenta.nombrePaciente = this.pacienteInfo.nombrePaciente+ ' ' + this.pacienteInfo.apellidoPaterno+ ' ' + this.pacienteInfo.apellidoMaterno
            this.infoVenta.sede = CEDE;
            this.infoVenta.prioridad = "Programado"
            this.infoVenta.fecha = dates.getDate();
            this.infoVenta.doctorQueSolicito = medico;
            /* this.carrito.items.push(this.item); */
            this.infoVenta.estudios= this.carrito.items
            this.infoVenta.totalCompra = 0;
            const item = {
              nombreEstudio: "BIOMETRIA HEMATICA COMPLETA",
              precioCon:0,
              precioSin:0,
              idEstudio:'5fd284b11cb0ea0017f57bd8'
            }
            this.carrito.items.push(item);
            this._pagoServicios.agregarPedido( this.infoVenta )
            .subscribe( (data) => {
              // console.log( data );
              if(  data['ok'] ){
                
                this.generarTicket(data['folio']);
                  // se crea la tabla de las ventas 
                  this.setDatesPedidos();
                  this.pedidosLaboratorios.estudios.push(item);
                  console.log(this.pedidosLaboratorios);
                  
                  this._pagoServicios.pedidosLaboratorio( this.pedidosLaboratorios ).subscribe( data => console.log( data ));
                  swal('Consulta Agregada', 'Puede ver las visitas en la tabla', 'success')

                  // ESTA INFORMCAIÓN SE ENVIA A LOS DIFERENTES SERVICIOS 
                  // seteamos las fechas 
                    eliminarStorage();
                    
                    
                    const eliminarPaciente = new PacienteStorage();
                    eliminarPaciente.removePacienteStorage();  
                }
            });
            this.mostrarConsultas();
        break;
      case '1 QS 6 (Glucosa, Urea, Creatinina, Ácido Úrico, Colesterol y Triglicéridos)':
        this.item.nombreEstudio = "QUIMICA SANGUINEA 6 ";
        this.item._id = '5fd2a4171cb0ea0017f57ddb';
        this.fecha = moment().format('l');    
        this.infoVenta.hora = moment().format('LT');
        this.infoVenta.vendedor = getDataStorage()._id;
        if(this.pacienteInfo.id == undefined){
          this.infoVenta.paciente = this.pacienteInfo._id;
        }else{
          this.infoVenta.paciente = this.pacienteInfo.id;
        }
        this.infoVenta.nombrePaciente = this.pacienteInfo.nombrePaciente+ ' ' + this.pacienteInfo.apellidoPaterno+ ' ' + this.pacienteInfo.apellidoMaterno
        this.infoVenta.sede = CEDE;
        this.infoVenta.prioridad = "Programado"
        this.infoVenta.fecha = dates.getDate();
        this.infoVenta.doctorQueSolicito = medico;
        /* this.carrito.items.push(this.item); */
        this.infoVenta.estudios= this.carrito.items
        this.infoVenta.totalCompra = 0;
        const item2 = {
          nombreEstudio: "QUIMICA SANGUINEA 6 ",
          precioCon:0,
          precioSin:0,
          idEstudio:'5fd2a4171cb0ea0017f57ddb'
        }
        this.carrito.items.push(item2);
        this._pagoServicios.agregarPedido( this.infoVenta )
        .subscribe( (data) => {
          // console.log( data );
          if(  data['ok'] ){
            
            // se crea la tabla de las ventas 
            this.generarTicket(data['folio']);
            this.setDatesPedidos();
                  this.pedidosLaboratorios.estudios.push(item2);
                  console.log(this.pedidosLaboratorios);
                  
                  this._pagoServicios.pedidosLaboratorio( this.pedidosLaboratorios ).subscribe( data => console.log( data ));
              swal('Consulta Agregada', 'Puede ver las visitas en la tabla', 'success')

              // ESTA INFORMCAIÓN SE ENVIA A LOS DIFERENTES SERVICIOS 
              // seteamos las fechas 
                eliminarStorage();
                
                
                const eliminarPaciente = new PacienteStorage();
                eliminarPaciente.removePacienteStorage();  
            }
        });
        this.mostrarConsultas();
      break;
      case '1 Pruebas de Funcionamiento Hepático':
        this.item.nombreEstudio = "PRUEBAS DE FUNCION HEPATICO (BT,BD,BI,TGO,TGP,ALK)";
        this.item._id = '5fd2992b1cb0ea0017f57d1e';
        this.fecha = moment().format('l');    
        this.infoVenta.hora = moment().format('LT');
        this.infoVenta.vendedor = getDataStorage()._id;
        if(this.pacienteInfo.id == undefined){
          this.infoVenta.paciente = this.pacienteInfo._id;
        }else{
          this.infoVenta.paciente = this.pacienteInfo.id;
        }
        this.infoVenta.nombrePaciente = this.pacienteInfo.nombrePaciente+ ' ' + this.pacienteInfo.apellidoPaterno+ ' ' + this.pacienteInfo.apellidoMaterno
        this.infoVenta.sede = CEDE;
        this.infoVenta.prioridad = "Programado"
        this.infoVenta.fecha = dates.getDate();
        this.infoVenta.doctorQueSolicito = medico;
        /* this.carrito.items.push(this.item); */
        this.infoVenta.estudios= this.carrito.items
        this.infoVenta.totalCompra = 0;
        const item3 = {
          nombreEstudio: "PRUEBAS DE FUNCION HEPATICO (BT,BD,BI,TGO,TGP,ALK)",
          precioCon:0,
          precioSin:0,
          idEstudio:'5fd2992b1cb0ea0017f57d1e'
        }
        this.carrito.items.push(item3);
        this._pagoServicios.agregarPedido( this.infoVenta )
        .subscribe( (data) => {
          // console.log( data );
          if(  data['ok'] ){
            
            this.generarTicket(data['folio']);
              // se crea la tabla de las ventas
              this.setDatesPedidos();
                  this.pedidosLaboratorios.estudios.push(item3);
                  console.log(this.pedidosLaboratorios);
                  
                  this._pagoServicios.pedidosLaboratorio( this.pedidosLaboratorios ).subscribe( data => console.log( data )); 
              swal('Consulta Agregada', 'Puede ver las visitas en la tabla', 'success')

              // ESTA INFORMCAIÓN SE ENVIA A LOS DIFERENTES SERVICIOS 
              // seteamos las fechas 
                eliminarStorage();
                
                
                const eliminarPaciente = new PacienteStorage();
                eliminarPaciente.removePacienteStorage();  
            }
        });
        this.mostrarConsultas();
      break;
      case '1 Exámenes Generales de Orina':
        this.item.nombreEstudio = "EXAMEN GENERAL DE ORINA ";
        this.item._id = '5fd28c2f1cb0ea0017f57c58';
        this.fecha = moment().format('l');    
        this.infoVenta.hora = moment().format('LT');
        this.infoVenta.vendedor = getDataStorage()._id;
        if(this.pacienteInfo.id == undefined){
          this.infoVenta.paciente = this.pacienteInfo._id;
        }else{
          this.infoVenta.paciente = this.pacienteInfo.id;
        }
        this.infoVenta.nombrePaciente = this.pacienteInfo.nombrePaciente+ ' ' + this.pacienteInfo.apellidoPaterno+ ' ' + this.pacienteInfo.apellidoMaterno
        this.infoVenta.sede = CEDE;
        this.infoVenta.prioridad = "Programado"
        this.infoVenta.fecha = dates.getDate();
        this.infoVenta.doctorQueSolicito = medico;
        /* this.carrito.items.push(this.item); */
        this.infoVenta.estudios= this.carrito.items
        this.infoVenta.totalCompra = 0;
        const item4 = {
          nombreEstudio: "EXAMEN GENERAL DE ORINA ",
          precioCon:0,
          precioSin:0,
          idEstudio:'5fd28c2f1cb0ea0017f57c58'
        }
        this.carrito.items.push(item4);
        this._pagoServicios.agregarPedido( this.infoVenta )
        .subscribe( (data) => {
          // console.log( data );
          if(  data['ok'] ){
            
            this.generarTicket(data['folio']);
              // se crea la tabla de las ventas 
              this.setDatesPedidos();
                  this.pedidosLaboratorios.estudios.push(item3);
                  console.log(this.pedidosLaboratorios);
                  
                  this._pagoServicios.pedidosLaboratorio( this.pedidosLaboratorios ).subscribe( data => console.log( data ));
              swal('Consulta Agregada', 'Puede ver las visitas en la tabla', 'success')

              // ESTA INFORMCAIÓN SE ENVIA A LOS DIFERENTES SERVICIOS 
              // seteamos las fechas 
                eliminarStorage();
                
                
                const eliminarPaciente = new PacienteStorage();
                eliminarPaciente.removePacienteStorage();  
            }
        });
        this.mostrarConsultas();
      break;
      case '1 Tele de Tórax.':
        this.item.nombreEstudio = "RADIOGRAFÍA TELE DE TÓRAX";
        this.item._id = '5fd4f80e8adaca00176bc5b8';
        this.fecha = moment().format('l');    
        this.infoVenta.hora = moment().format('LT');
        this.infoVenta.vendedor = getDataStorage()._id;
        if(this.pacienteInfo.id == undefined){
          this.infoVenta.paciente = this.pacienteInfo._id;
        }else{
          this.infoVenta.paciente = this.pacienteInfo.id;
        }
        this.infoVenta.nombrePaciente = this.pacienteInfo.nombrePaciente+ ' ' + this.pacienteInfo.apellidoPaterno+ ' ' + this.pacienteInfo.apellidoMaterno
        this.infoVenta.sede = CEDE;
        this.infoVenta.prioridad = "Programado"
        this.infoVenta.fecha = dates.getDate();
        this.infoVenta.doctorQueSolicito = medico;
        /* this.carrito.items.push(this.item); */
        this.infoVenta.estudios= this.carrito.items
        this.infoVenta.totalCompra = 0;
        const item5 = {
          nombreEstudio: "RADIOGRAFÍA TELE DE TÓRAX",
          precioCon:0,
          precioSin:0,
          idEstudio:'5fd4f80e8adaca00176bc5b8'
        }
        this.carrito.items.push(item5);
        this._pagoServicios.agregarPedido( this.infoVenta )
        .subscribe( (data) => {
          // console.log( data );
          if(  data['ok'] ){
            
            this.generarTicket(data['folio']);
              // se crea la tabla de las ventas
              this.setDatesPedidos();
              this.pedidosRayox.estudios.push(item5)
              this._pagoServicios.postPedidosXray(this.pedidosRayox).subscribe(data => console.log(data)); 
              swal('Consulta Agregada', 'Puede ver las visitas en la tabla', 'success')

              // ESTA INFORMCAIÓN SE ENVIA A LOS DIFERENTES SERVICIOS 
              // seteamos las fechas 
                eliminarStorage();
                
                
                const eliminarPaciente = new PacienteStorage();
                eliminarPaciente.removePacienteStorage();  
            }
        });
        this.mostrarConsultas();
      break;
      case '1 Electrocardiograma':
        this.item.nombreEstudio = "ELECTROCARDIOGRAMA";
        this.item._id = '5fd7c35961c8830017d478b3';
        this.fecha = moment().format('l');    
        this.infoVenta.hora = moment().format('LT');
        this.infoVenta.vendedor = getDataStorage()._id;
        if(this.pacienteInfo.id == undefined){
          this.infoVenta.paciente = this.pacienteInfo._id;
        }else{
          this.infoVenta.paciente = this.pacienteInfo.id;
        }
        this.infoVenta.nombrePaciente = this.pacienteInfo.nombrePaciente+ ' ' + this.pacienteInfo.apellidoPaterno+ ' ' + this.pacienteInfo.apellidoMaterno
        this.infoVenta.sede = CEDE;
        this.infoVenta.prioridad = "Programado"
        this.infoVenta.fecha = dates.getDate();
        this.infoVenta.doctorQueSolicito = medico;
        /* this.carrito.items.push(this.item); */
        this.infoVenta.estudios= this.carrito.items
        this.infoVenta.totalCompra = 0;
        const item6 = {
          nombreEstudio: "ELECTROCARDIOGRAMA",
          precioCon:0,
          precioSin:0,
          idEstudio:'5fd7c35961c8830017d478b3'
        }
        this.carrito.items.push(item6);
        this._pagoServicios.agregarPedido( this.infoVenta )
        .subscribe( (data) => {
          // console.log( data );
          if(  data['ok'] ){
            
            this.generarTicket(data['folio']);
              // se crea la tabla de las ventas 

              swal('Consulta Agregada', 'Puede ver las visitas en la tabla', 'success')

              // ESTA INFORMCAIÓN SE ENVIA A LOS DIFERENTES SERVICIOS 
              // seteamos las fechas 
                eliminarStorage();
                
                
                const eliminarPaciente = new PacienteStorage();
                eliminarPaciente.removePacienteStorage();  
            }
        });
        this.mostrarConsultas();
      break;
      default:
        break;
    }
  }

  setDatesPedidos (){
    // this.pedidosLaboratorios.fecha = moment().format('l');
    const datesPedidoLab = new Dates();
    // configuracion de los pedidos de laboratorio
    this.pedidosLaboratorios.fecha = datesPedidoLab.getDate();
    this.pedidosLaboratorios.hora = moment().format('LT');
    this.pedidosLaboratorios.medicoQueSolicita = this.infoVenta.doctorQueSolicito;
    this.pedidosLaboratorios.sede = CEDE;
    this.pedidosLaboratorios.idPaciente = this.infoVenta.paciente;
    this.pedidosLaboratorios.idPaciente = this.infoVenta.paciente;
    // configuracion de los pedidos de ultrasonido
    this.pedidosUltrasonido.idPaciente = this.infoVenta.paciente;
    this.pedidosUltrasonido.fecha = datesPedidoLab.getDate();
    this.pedidosUltrasonido.sede = CEDE;
    //rayosx
    this.pedidosRayox.idPaciente = this.infoVenta.paciente;
    this.pedidosRayox.fecha = datesPedidoLab.getDate();
    this.pedidosRayox.medicoQueSolicita = this.infoVenta.doctorQueSolicito;
    this.pedidosRayox.sede = CEDE;
  }

}

class consultas  {

  tipo:  string;
  consulta:  string;
  fecha:  string;
  firma:  string;
  hora:  string;
  medico:  string;

}
