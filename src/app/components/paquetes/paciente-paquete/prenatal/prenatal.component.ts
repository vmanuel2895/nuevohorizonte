import { Component, OnInit, Input } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { PaquetesService } from '../../../../services/paquetes/paquetes.service';
import * as moment from 'moment';
import swal from 'sweetalert';
import { FichaInfo } from 'src/app/classes/ficha-info-paciente';
import { PagoServiciosService } from 'src/app/services/pagos/pago-servicios.service';
import Dates from 'src/app/classes/dates/date.class';
import Tickets from 'src/app/classes/tickets/ticket.class';
import { getDataStorage } from 'src/app/functions/storage.funcion';
import { CEDE } from 'src/app/classes/cedes/cedes.class';
import { eliminarStorage } from 'src/app/functions/pacienteIntegrados';
import PacienteStorage from 'src/app/classes/pacientes/pacientesStorage.class';

@Component({
  selector: 'app-prenatal',
  templateUrl: './prenatal.component.html',
  styleUrls: ['./prenatal.component.css']
})
export class PrenatalComponent implements OnInit {

    @Input() id: String;
    consultas:any = { tipo: '', consulta: '', fecha: '', hora:'' , medico: '', firma: '' }
    item:any ={ nombreEstudio:'', precioCon:0, precioSin:0, _id:''}
    medicos:any[] = []
    paquete:any[] = []
    concepto:any[] = []
    citas:any[] = []
    examenes:any[] = []
    totalpagos:number
    pagos:any = { semanaGesta:'', semanapago: '', pago: ''}
    public deposito =0
    tabularpagos:any = []
    public egoCount=0;
    public BHC = 0;
    public QS = 0;
    public EGO = 0;
    public VDRL = 0;
    public GS = 0;
    public VIH = 0;
    public TC = 0;
    public CTG=0;
    public EV =0;
    public URO =0;
    public contador=0;
    public contadorcitas=0;
    public GINE = 0;
    public hospitalizacion = {fecha:"", hora:"" , consulta:"", medico: "", firma:"" };
    public consultaDespuesParto = {fecha:"", hora:"" , consulta:"", medico: "", firma:"" };
    // public paqueteExamenesInto = [];
    public fechaConsulta = moment().format('l');
    public horaIngreso = moment().format('hh:mm');
    public btnAgregaConsultas = false;
    public pacienteFicha: FichaInfo;
    
    public pacienteInfo={
      nombrePaciente: "",
      apellidoPaterno: "",
      apellidoMaterno: "",
      curp: "",
      edad: 0,
      genero: "",
      id:"",
      callePaciente: "",
      fechaNacimientoPaciente:"",
      estadoPaciente: "",
      paisPaciente: "",
      telefono: "",
      _id:""
    };

    public sem = [];
    public infoVenta = {  

      paciente:"",
      nombrePaciente:"",
      vendedor:"",
      fecha:"",
      hora:"",
      estudios:[],
      efectivo:false,
      doctorQueSolicito:"",
      transferencia: false,
      tarjetCredito:false,
      tarjetaDebito:false,
      montoEfectivo:0,
      montoTarjteCredito:0,
      montoTarjetaDebito:0,
      montoTranferencia: 0,
      sede:"",
      totalCompra:0,
      prioridad:"",
      compraConMembresia:true
  
    }
  
    public carrito = {
      totalSin: 0,
      totalCon: 0,
      items: []
    };
    
    fecha: string;
    public semanaGestacion=[];
    public semanapago=[];

    public pedidosLaboratorios = { 
      estudios:[],
      idPaciente:"",
      fecha:"",
      hora:"",
      medicoQueSolicita:"",
      sede:"",
      prioridad:"Programado",
      estado:""
    }

    public pedidosUltrasonido = {
      idPaciente:"", 
      estudios:[],
      fecha:"",
      hora:"",
      medicoQueSolicita:"",
      sede:"",
      prioridad:"Programado"
    }

    public OBSTETRI=0;
    public ESTRUC = 0;
    public PEDI= 0;
    public comparacion;

  constructor(public _router: ActivatedRoute, public _paquete:PaquetesService, public _route:Router, private _pagoServicios: PagoServiciosService) {
    // this.obtenerPaquete();
  } 
  
  ngOnInit(): void {

    this.obtenerPaquete();
    this.obtenerMedicos();
 /* this.validacionescitas() */
    this.consultas.firma = JSON.parse(localStorage.getItem('usuario')).nombre;
  }
//////////////////// calculo de pagos y semanas de pagos automaticos //////////////////////////////

  obtenerPagos(){
    this.pagos.pago =(this.deposito + (15000 /this.pagos.semanapago)).toFixed(2)
    this.comparacion = this.pagos.pago
  }

  obtenerSemanasPago(){
    console.log(this.pagos);
    
    if(this.pagos.semanaGesta == undefined){
      this.pagos.semanaGesta = '1';
    }
    this.pagos.semanapago = (39 - this.pagos.semanaGesta)
    for (let index = this.pagos.semanapago; index < 39; index++) {
      this.semanapago.push(index)
    }
  }
  ///////////////////////////////////////////////////////////////7
  obtenerPaquete(){
    this._paquete.obtenerPaquete(this.id)
    .subscribe(  (data:any) =>  {  
      // obtenemos la tabla intermedia del control de las visitas 5e683cf41c9d4400005e1727
      console.log(data);
      
      this.pacienteInfo = data['paquetes'][0]['paciente'];
      this.paquete = data['paquetes']
      this.citas = this.paquete[0].visitas
      this.examenes = this.paquete[0].examenesLab;
      this.tabularpagos = this.paquete[0].pagos;
      this.semanasdegestacion(this.tabularpagos);
      console.log( this.pacienteInfo );
      console.log( this.tabularpagos );
      this.totalpagos = (0+2500)
      //console.log(this.pagos);
      this.tabularpagos = this.tabularpagos.reverse(); 
      this.tabularpagos.forEach(element => {
        this.totalpagos=element.pago+this.totalpagos
        console.log(this.totalpagos);
        
      });
      // this.pacienteInfo = this.pacienteFicha;
      this.verCosnultashechas();
    });
  }

  semanasdegestacion(semanas){
    const semana = semanas.reverse();
    if(semanas.length == 0){
      this.sem.push(this.pagos)
      this.generararreglo(this.sem);
    }else{
      this.sem.push(semana[0]);
      this.generararreglo(this.sem);
    }
    
  }
  generararreglo(sem:any){
    if(sem[0].semanaGesta == ""){
      for (let index = 1; index < 39; index++) {
        this.semanaGestacion.push(index);
      }
    }else{
      const semana = parseInt(sem[0].semanaGesta)+1;
      for (let index = semana; index < 39; index++) {
        this.semanaGestacion.push(index)
      }
    }
  }
  obtenerMedicos(){
    this._paquete.getMedicos()
    .subscribe( (data) => {
      this.medicos = data['medicos']
    });
  }

  irAUnServicio(  servicio ){
    this.setRouteService( servicio );
  }

  setRouteService(servicio){
    this._route.navigate([ `/serviciosInt/${servicio}`]);
  }

  verCosnultashechas(){
    this. GINE = 0;
    this.OBSTETRI=0;
    this.ESTRUC = 0;
    this.PEDI= 0;
    // esta funcion se encarga de ver cuales de los paquetes ya estan hechas y las colorea
    let cardHospitalizacion = document.querySelector('#hospitalizacion');
    let badegHospitalizacion = document.querySelector('#badgeHospitalzacion');
    // consultas despues del parto
    let cardConsultas = document.querySelector('#consultaDespuesDelParto');
    let badgeConsultas = document.querySelector('#badgeParto');
    // incia el codifo que valida las consultas 
    this.citas.forEach(element => {
      // console.log( element );
      if( element.consulta == "Hospitalicación en habitación estándar" ){
        // console.log("Se lo lee");
        cardHospitalizacion.classList.add('border-primary');
        badegHospitalizacion.innerHTML = "1";
        badegHospitalizacion.classList.add('badge-primary');
        this.hospitalizacion = element ;
      } 
      if( element.consulta === 'Consulta despues del parto o Cesárea con Medicina General' ){
        cardConsultas.classList.add('border-primary');
        badgeConsultas.innerHTML = '1';
        badgeConsultas.classList.add('badge-primary');
        this.consultaDespuesParto = element;
      }
      if(element.consulta == '7 Consultas con especialista en Ginecología y obstetricia'){
        this.GINE += 1;
        let cardBiometria = document.querySelector('#gineco');
        let badgeBiometrie = document.querySelector('#badegeGineco');
        this.addClassesCss( cardBiometria, badgeBiometrie);
      }
      if(element.consulta == '2 Ultrasonidos obstétricos convencionales'){
        this.OBSTETRI += 1;
        let cardBiometria = document.querySelector('#cardObstetri');
        let badgeBiometrie = document.querySelector('#badgeObstetri');
        this.addClassesCss( cardBiometria, badgeBiometrie);
      }
      if(element.consulta == '1 Ultrasonido estructural'){
        this.ESTRUC += 1;
        let cardBiometria = document.querySelector('#estructuralCard');
        let badgeBiometrie = document.querySelector('#badgeEstructural');
        this.addClassesCss( cardBiometria, badgeBiometrie);
      }
      if(element.consulta == '1 consulta con pediatría antes de su parto o cesárea'){
        this.PEDI += 1;
        let cardBiometria = document.querySelector('#pediatria');
        let badgeBiometrie = document.querySelector('#badegePediatria');
        this.addClassesCss( cardBiometria, badgeBiometrie);
      }
    });
    // termian el codigo que valida las consultas 
    this.verEstudios();
  }

  verEstudios(){
    this.egoCount=0;
    this. BHC = 0;
    this. QS = 0;
    this. EGO = 0;
    this. VDRL = 0;
    this. GS = 0;
    this. VIH = 0;
    this. TC = 0;
    this. CTG=0;
    // badgeQuimica
   //   // quimicaSanguineaCard
   for (const iterator of this.examenes) {
     /* }
    this.examenes.forEach( (examen:consultas) => { */
      if( iterator.consulta === "2 Biometría hemática completa" ){
        this.BHC += 1;
        console.log(this.BHC);
        
        let cardBiometria = document.querySelector('#cardBiometria');
        let badgeBiometrie = document.querySelector('#badgeBiometria');
        this.addClassesCss( cardBiometria, badgeBiometrie);
      }
      if( iterator.consulta === "1 Química sanguínea de 6 elementos (Glucosa, Urea, Creatinina, Ácido Úrico, Colesterol y Triglicéidos )"  ){
        this.QS += 1;
        let cardQuimica = document.querySelector('#quimicaSanguineaCard');
        let badgeQuimica = document.querySelector("#badgeQuimica");
        this.addClassesCss( cardQuimica, badgeQuimica);
      }
      if( iterator.consulta === "2 Exámen General de Orina"  ){
        this.EGO += 1;
        let egoCard = document.querySelector('#egoCard');
        let badgeEgo = document.querySelector("#badegeEgo1");
        this.addClassesCss( egoCard, badgeEgo);

      }
      if(iterator.consulta === "1 V.D.R.L"){
        this.VDRL += 1;
        let cardVrl = document.querySelector('#VrlCard');
        let badgeVrl = document.querySelector('#badgeVrl');
        this.addClassesCss( cardVrl, badgeVrl);
      }
      if( iterator.consulta === "1 Grupo Sanguíneo"  ){
        this.GS += 1;
        let grupoSanguineo = document.querySelector("#grupoSanguineoCard");
        let badgeSanguineo = document.querySelector("#badgeGrupoSanguineo");
        this.addClassesCss(grupoSanguineo, badgeSanguineo)
      }
      if( iterator.consulta === "1 V.I.H." ){
        this.VIH += 1;
        let vihcard = document.querySelector('#vihcard');
        let vihbadge = document.querySelector('#vihbadge');
        this.addClassesCss( vihcard, vihbadge);
      }
      if(  iterator.consulta === "1 Tiempos de Coagulación" ){
        this.TC += 1;
        let tiemposCoagulacionCard = document.querySelector("#tiemposCoagulacionCard");
        let  tiemposCoagulacionBadge = document.querySelector('#tiemposCoagulacionBadge');
        this.addClassesCss( tiemposCoagulacionCard, tiemposCoagulacionBadge);
      }
      if(  iterator.consulta === "1 Curva de tolerancia a la Glucosa"  ){
        this.CTG += 1;
        let curvaDetoleranciaCard = document.querySelector("#curvaDetoleranciaCard");
        let curvaDetoleranciaBadge = document.querySelector("#curvaDetoleranciaBadge");
        this.addClassesCss( curvaDetoleranciaCard, curvaDetoleranciaBadge);
      }
      if(  iterator.consulta === "1 Exudado Vaginal"  ){
        this.EV += 1;
        let curvaDetoleranciaCard = document.querySelector("#ExudadoVagCard");
        let curvaDetoleranciaBadge = document.querySelector("#badegeEV");
        this.addClassesCss( curvaDetoleranciaCard, curvaDetoleranciaBadge);
      }
      if(  iterator.consulta === "1 Urocultivo"  ){
        this.URO += 1;
        let curvaDetoleranciaCard = document.querySelector("#uroCard");
        let curvaDetoleranciaBadge = document.querySelector("#badegeURO");
        this.addClassesCss( curvaDetoleranciaCard, curvaDetoleranciaBadge);
      }
    };
   //   //  termina el codigo que valida los examenes 
  }

  showMessage(examen: string){
    this.examenes.forEach(  (estudio:consultas, index) => {
      // console.log( estudio.consulta  === examen  );
      // console.log( estudio  );
        if( estudio.consulta ===  examen  ){
          // console.log(estudio.consulta, index);
          swal( `Tipo:  ${this.examenes[index].consulta}\n  Fecha: ${this.examenes[index].fecha} \n Hora: ${this.examenes[index].hora}\n   Médico ${this.examenes[index].medico}\n  Atendió: ${this.examenes[index].firma}` );
        }
    });
  }

  addClassesCss( card: Element, badge:Element){
    card.classList.add('border-primary');
    if( badge.id == "badegeGineco"  ){
      badge.innerHTML = this.GINE.toString(); 
    }
    if( badge.id == "badegeEgo1"  ){
      badge.innerHTML = this.EGO.toString(); 
    }
    if(badge.id == 'badgeBiometria'){
      badge.innerHTML = this.BHC.toString();
    }
    if(badge.id == 'badgeQuimica'){
      badge.innerHTML = this.QS.toString();
    }
    if(badge.id == 'badgeVrl'){
      badge.innerHTML = this.VDRL.toString();
    }
    if(badge.id == 'badgeGrupoSanguineo'){
      badge.innerHTML = this.GS.toString();
    }
    if(badge.id == 'vihbadge'){
      badge.innerHTML = this.VIH.toString();
    }
    if(badge.id == 'tiemposCoagulacionBadge'){
      badge.innerHTML = this.TC.toString();
    }
    if(badge.id == 'curvaDetoleranciaBadge'){
      badge.innerHTML = this.CTG.toString();
    }
    if(badge.id == 'curvaDetoleranciaBadge'){
      badge.innerHTML = this.CTG.toString();
    }
    if(badge.id == 'badegeEV'){
      badge.innerHTML = this.EV.toString();
    }
    if(badge.id == 'badegeURO'){
      badge.innerHTML = this.URO.toString();
    }
    if(badge.id == 'badgeObstetri'){
      badge.innerHTML = this.OBSTETRI.toString();
    }
    if(badge.id == 'badgeEstructural'){
      badge.innerHTML = this.ESTRUC.toString();
    }
    if(badge.id == 'badegePediatria'){
      badge.innerHTML = this.PEDI.toString();
    }
    /* else {
      badge.innerHTML = "1";
    } */
    badge.classList.add('badge-primary');
  } 

  mostrarHospitalizacion(){
    console.log( this.hospitalizacion );
    swal( `Tipo:  ${this.hospitalizacion.consulta}\n  Fecha: ${this.hospitalizacion.fecha} \n Hora: ${this.hospitalizacion.hora}\n   Médico ${this.hospitalizacion.medico}\n  Atendió: ${this.hospitalizacion.firma}` );
  }

  cambiarVisita( selectCon ){
    let nombreConsultaSelect = selectCon.value ;
    if(this.consultas.tipo  === 'examenesLab' ){
      // let log = console.log;
      // checar si nos se han consumido los laboratorios
      this.examenes.forEach((examen:consultas) =>  {
          if( nombreConsultaSelect == examen.consulta && examen.consulta == nombreConsultaSelect ){
            swal("Este elemento ya ha sido usado",{icon:"error"})
            /* alert('Este elemento ya ha sido usado'); */
            // console.log("Biometria hematica");
            this.btnAgregaConsultas = true;
          }
      });
    }
    
    this.citas.forEach( element => {
      if( nombreConsultaSelect ===  element.consulta && element.consulta === "Hospitalicación en habitación estándar"  ){
        swal("Este elemento ya ha sido usado",{icon:"error"})
        this.btnAgregaConsultas = true;
      }else if(nombreConsultaSelect ===  element.consulta && element.consulta === "Consulta despues del parto o Cesárea con Medicina General"){
        this.btnAgregaConsultas = true;
        swal("Este elemento ya ha sido usado",{icon:"error"})
      }
      this.btnAgregaConsultas = false;
      if(  nombreConsultaSelect  != "Hospitalicación en habitación estándar" && nombreConsultaSelect  != "Consulta despues del parto o Cesárea con Medicina General" && nombreConsultaSelect != "1 Biometría hemática completa" && nombreConsultaSelect != "Quimica sanguinea de 6 elementos (Glucosa, Urea, Creatinina, Ácido Úrico, Colesterol y Trigliceridos )"  && nombreConsultaSelect != "1 V.D.R.L" && nombreConsultaSelect != "1 V.I.H." && nombreConsultaSelect != "1 Grupo Sanguineo" && nombreConsultaSelect != "1 Tiempos de Coagulación"  ){
        this.btnAgregaConsultas = false;        
      }
      if( this.egoCount == 2  ){
        this.btnAgregaConsultas = false;
      }
    }) 
  }

  //insercion
  seleccion($event, value){
    switch (value) {
      case 'visitas':
        this.concepto=[]
        for(let item of this.paquete ){
          for(let item2 of item.paquete.CitasIncluidas){
            this.concepto.push(item2)
          }
          console.log(this.concepto)
        }  
        this.consultas.consulta = ''
      break;

      case 'examenesLab':
        this.concepto=[]
        for(let item of this.paquete ){
          for(let item2 of item.paquete.examenesLaboratorio){
            this.concepto.push(item2)
          }
          console.log(this.concepto)
        }  
        this.consultas.consulta = ''
      break;

      default:
        break;
    }
  }

  agregarConsulta(){
    this.consultas.fecha = this.fechaConsulta;
    this.consultas.hora = this.horaIngreso;
    this.consultas.firma = JSON.parse(localStorage.getItem('usuario')).nombre;
    console.log(this.consultas);
    
    if(this.consultas.tipo == '' || this.consultas.consulta == '' || this.consultas.medico == '' || this.consultas.firma == ''){
      swal('Error!', 'Porfavor ingrese los datos que le piden', 'error')
      console.log(this.citas.length);
      console.log(this.citas);
      
    }else{
      if(this.consultas.tipo == 'visitas'){
        if(this.consultas.consulta == '7 Consultas con especialista en Ginecología y obstetricia'){
          let valu = true;
          valu = this.validacionescitas();
          if(valu){
            this._paquete.agregarConsulta(this.consultas,this.consultas.tipo,this.id).subscribe(
              (data)=>{
                //swal('Consulta Agregada', 'Puede ver las visitas en la tabla', 'success')
                this.mostrarConsultas();
                this.setDatesVenta(this.consultas.medico);
                this._pagoServicios.agregarPedido( this.infoVenta )
                .subscribe( (data) => {
                  // console.log( data );
                  if(  data['ok'] ){
                    
                    this.generarTicket(data['folio']);
                      // se crea la tabla de las ventas 
                      this.obtenerPaquete();
                      swal('Consulta Agregada', 'Puede ver las visitas en la tabla', 'success')
      
                      // ESTA INFORMCAIÓN SE ENVIA A LOS DIFERENTES SERVICIOS 
                      // seteamos las fechas 
                        eliminarStorage();
                        
                        
                        const eliminarPaciente = new PacienteStorage();
                        eliminarPaciente.removePacienteStorage(); 
                        this.consultas = { tipo: '', consulta: '', fecha: '', medico: '', firma:'' } 
                        this._route.navigateByUrl('consulta'); 
                    }
                });
              })
          }else{
            swal('Error!','Se terminaron las consultas de tu paquete.', 'error')
            this.consultas = { tipo: '', consulta: '', fecha: '', medico: '', firma:'' }
          }
        }else if(this.consultas.consulta == '1 consulta con pediatría antes de su parto o cesárea'){
          let valu = true;
          valu = this.validacionescitas();
          if(valu){
            this._paquete.agregarConsulta(this.consultas,this.consultas.tipo,this.id).subscribe(
              (data)=>{
                //swal('Consulta Agregada', 'Puede ver las visitas en la tabla', 'success')
                this.mostrarConsultas();
                this.setDatesVenta(this.consultas.medico);
                this._pagoServicios.agregarPedido( this.infoVenta )
                .subscribe( (data) => {
                  // console.log( data );
                  if(  data['ok'] ){
                    
                    this.generarTicket(data['folio']);
                      // se crea la tabla de las ventas 
                      this.obtenerPaquete();
                      swal('Consulta Agregada', 'Puede ver las visitas en la tabla', 'success')
      
                      // ESTA INFORMCAIÓN SE ENVIA A LOS DIFERENTES SERVICIOS 
                      // seteamos las fechas 
                        eliminarStorage();
                        
                        
                        const eliminarPaciente = new PacienteStorage();
                        eliminarPaciente.removePacienteStorage(); 
                        this.consultas = { tipo: '', consulta: '', fecha: '', medico: '', firma:'' } 
                        this._route.navigateByUrl('consulta'); 
                    }
                });
              })
          }else{
            swal('Error!','Se terminaron las consultas de tu paquete.', 'error')
            this.consultas = { tipo: '', consulta: '', fecha: '', medico: '', firma:'' }
          }
          
        }else if(this.consultas.consulta == '2 Ultrasonidos obstétricos convencionales'){
          let val = true;
        val = this.validacionescitas();
        console.log(val);
        
        if(val){
          this._paquete.agregarConsulta(this.consultas,this.consultas.tipo,this.id).subscribe(
            (data)=>{
              this.setDatesVenta(this.consultas.medico);          
              this.pagar_consulta(this.consultas.consulta, this.consultas.medico);
              this.consultas = { tipo: '', consulta: '', fecha: '', medico: '', firma:'' }
            })
        }else{
          swal('Error!','Se terminaron los laboratorios de tu paquete.', 'error')
          this.consultas = { tipo: '', consulta: '', fecha: '', medico: '', firma:'' }
        }
          
        }else if(this.consultas.consulta == '1 Ultrasonido estructural'){
          console.log('entro estructural');
          
        }
        
      }else{
        let val = true;
        val = this.validaciones();
        console.log(val);
        
        if(val){
          this._paquete.agregarConsulta(this.consultas,this.consultas.tipo,this.id).subscribe(
            (data)=>{
              this.setDatesVenta(this.consultas.medico);          
              this.pagar_consulta(this.consultas.consulta, this.consultas.medico);
              this.consultas = { tipo: '', consulta: '', fecha: '', medico: '', firma:'' }
            })
        }else{
          swal('Error!','Se terminaron los laboratorios de tu paquete.', 'error')
          this.consultas = { tipo: '', consulta: '', fecha: '', medico: '', firma:'' }
        }
      }

        //citas   
    }
  }

  validacionescitas(){
    this.contadorcitas=0;
    switch(this.consultas.consulta){
      case '7 Consultas con especialista en Ginecología y obstetricia':
        for (const iterator of this.citas) {
            if(iterator.consulta == '7 Consultas con especialista en Ginecología y obstetricia'){
              this.contadorcitas += 1
            }
          }  
          if(this.contadorcitas < 7){
            return true;
          }else{
            return false;
          }
        break;
      case '1 consulta con pediatría antes de su parto o cesárea':
        for (const iterator of this.citas) {
          if(iterator.consulta == '1 consulta con pediatría antes de su parto o cesárea'){
            this.contadorcitas += 1
          }
          console.log(this.contadorcitas);
          }  
          if(this.contadorcitas < 1){
            return true;
          }else{
            return false;
          }
        break;
      case '2 Ultrasonidos obstétricos convencionales':
          for (const iterator of this.citas) {
            if(iterator.consulta == '2 Ultrasonidos obstétricos convencionales'){
              this.contadorcitas += 1
            }
            console.log(this.contadorcitas);
            }  
            if(this.contadorcitas < 2){
              return true;
            }else{
              return false;
            }
          break;
      case '1 Ultrasonido estructural':
        for (const iterator of this.citas) {
          if(iterator.consulta == '1 Ultrasonido estructural'){
            this.contadorcitas += 1
          }
          console.log(this.contadorcitas);
          }  
          if(this.contadorcitas < 1){
            return true;
          }else{
            return false;
          }
        break;
      
    }
  }

  validaciones(){
    this.contador=0
    console.log(this.examenes);
    switch(this.consultas.consulta){      
      case '2 Biometría hemática completa':
        for (const iterator of this.examenes) {
          if(iterator.consulta == '2 Biometría hemática completa'){
            this.contador= this.contador + 1
          }
        }
        if(this.contador < 2){
          return true;
        }else{
          return false;
        }
        break;
      case '1 Química sanguínea de 6 elementos (Glucosa, Urea, Creatinina, Ácido Úrico, Colesterol y Triglicéidos )':
          for (const iterator of this.examenes) {
            if(iterator.consulta == '1 Química sanguínea de 6 elementos (Glucosa, Urea, Creatinina, Ácido Úrico, Colesterol y Triglicéidos )'){
              this.contador= this.contador + 1
            }
          }
          console.log('asghasgdh');
          
          if(this.contador < 1){
            return true;
          }else{
            return false;
          }
        break;
      case '2 Exámen General de Orina':
          for (const iterator of this.examenes) {
            if(iterator.consulta == '2 Exámen General de Orina'){
              this.contador= this.contador + 1
            }
          }
          console.log(this.contador);
          
          if(this.contador < 2){
            return true;
          }else{
            return false;
          }
        break;
      case '1 V.D.R.L':
          for (const iterator of this.examenes) {
            if(iterator.consulta == '1 V.D.R.L'){
              this.contador= this.contador + 1
            }
          }
          if(this.contador < 1){
            return true;
          }else{
            return false;
          }
        break;
      case '1 V.I.H.':
          for (const iterator of this.examenes) {
            if(iterator.consulta == '1 V.I.H.'){
              this.contador= this.contador + 1
            }
          }
          if(this.contador < 1){
            return true;
          }else{
            return false;
          }
        break;
      case '1 Grupo Sanguíneo':
          for (const iterator of this.examenes) {
            if(iterator.consulta == '1 Grupo Sanguíneo'){
              this.contador= this.contador + 1
            }
          }
          if(this.contador < 1){
            return true;
          }else{
            return false;
          }
        break;
      case '1 Tiempos de Coagulación':
          for (const iterator of this.examenes) {
            if(iterator.consulta == '1 Tiempos de Coagulación'){
              this.contador= this.contador + 1
            }
          }
          if(this.contador < 1){
            return true;
          }else{
            return false;
          }
        break;
        case '1 Curva de tolerancia a la Glucosa':
          for (const iterator of this.examenes) {
            if(iterator.consulta == '1 Curva de tolerancia a la Glucosa'){
              this.contador= this.contador + 1
            }
          }
          if(this.contador < 1){
            return true;
          }else{
            return false;
          }
        break;
        case '1 Exudado Vaginal':
          for (const iterator of this.examenes) {
            if(iterator.consulta == '1 Exudado Vaginal'){
              this.contador= this.contador + 1
            }
          }
          if(this.contador < 1){
            return true;
          }else{
            return false;
          }
        break;
        case '1 Urocultivo':
          for (const iterator of this.examenes) {
            if(iterator.consulta == '1 Urocultivo'){
              this.contador= this.contador + 1
            }
          }
          if(this.contador < 1){
            return true;
          }else{
            return false;
          }
        break;
    }
  }

  //metodo 
  setDatesVenta(medico){
    if(this.consultas.consulta == '7 Consultas con especialista en Ginecología y obstetricia'){
      const item = {
        nombreEstudio: "CONSULTA GINECOLOGIA",
        precioCon:0,
        precioSin:0,
        _id:'5fdd2b41a71eca0017a8e936'
      }
      this.carrito.items.push(item);
    }else if('1 consulta con pediatría antes de su parto o cesárea'){
      const item = {
        nombreEstudio: "CONSULTA PEDIATRIA",
        precioCon:0,
        precioSin:0,
        _id:'5fdd2bfba71eca0017a8e939'
      }
      this.carrito.items.push(item);
    }else{
      const item = {
        nombreEstudio: "CONSULTA DE MEDICO GENERAL",
        precioCon:0,
        precioSin:0,
        _id:'5fd3ebca08ccbb0017712f0d'
      }
      this.carrito.items.push(item);
    }
    
    const dates = new Dates();
    //this.infoVenta.totalCompra = this.carrito.totalSin;
    this.fecha = moment().format('l');    
    this.infoVenta.hora = moment().format('LT');
    this.infoVenta.vendedor = getDataStorage()._id;
    if(this.pacienteInfo.id == undefined){
      this.infoVenta.paciente = this.pacienteInfo._id;
    }else{
      this.infoVenta.paciente = this.pacienteInfo.id;
    }
    this.infoVenta.sede = CEDE;
    this.infoVenta.prioridad = "Programado"
    this.infoVenta.fecha = dates.getDate();
    this.infoVenta.doctorQueSolicito = medico;
    this.infoVenta.estudios= this.carrito.items
    this.infoVenta.totalCompra = 0;    
  }

  agregarPago(){
    console.log(this.pagos.pago)
    console.log(parseFloat(this.comparacion));
    
    if(this.pagos.tipo == '' || this.pagos.pago == '' || this.pagos.semanaGesta == '' || this.pagos.semanapago == ''){
      swal('Error!', 'Porfavor ingrese los datos que le piden', 'error')
    }else{
      if(this.pagos.pago < parseFloat(this.comparacion)){
        swal('Error!', 'Porfavor ingrese un monto mayor al pago correspondiente de la semana', 'error')
        this.obtenerPagos();
      }else{
        this.pagos.pago = parseFloat(this.pagos.pago);      
        this._paquete.agregarConsulta(this.pagos,'pagos',this.id).subscribe((data)=>{
        this.carrito={
          totalSin: parseFloat(this.pagos.pago),
          totalCon: parseFloat(this.pagos.pago),
          items: []
        };
        const item8 = {
          nombreEstudio: "Pago semana"+this.pagos.semanapago,
          precioCon:parseFloat(this.pagos.pago),
          precioSin:parseFloat(this.pagos.pago),
        }
        this.carrito.items.push(item8);
        this.generarTicket(400);
        this.pagos = { semanaGesta:'', semanapago: '', pago: ''}
        this.carrito={
          totalSin: 0,
          totalCon: 0,
          items: []
        };
        this.sem = [];
        this.obtenerPaquete();
        swal('Pago Agregada', 'se agrego el pago', 'success');
        /* this.ngOnInit(); */
      })
      // movi este codigo para recargar lo anterior
      // this.pagos = {semanaGesta:'', semanapago: '', pago: ''}
      this.deposito=0;
      }
    }
  }

  setDatesPedidos (){
    // this.pedidosLaboratorios.fecha = moment().format('l');
    const datesPedidoLab = new Dates();
    // configuracion de los pedidos de laboratorio
    this.pedidosLaboratorios.fecha = datesPedidoLab.getDate();
    this.pedidosLaboratorios.hora = moment().format('LT');
    this.pedidosLaboratorios.medicoQueSolicita = this.infoVenta.doctorQueSolicito;
    this.pedidosLaboratorios.sede = CEDE;
    this.pedidosLaboratorios.idPaciente = this.infoVenta.paciente;

    // configuracion de los pedidos de ultrasonido
    this.pedidosUltrasonido.idPaciente = this.infoVenta.paciente;
    this.pedidosUltrasonido.fecha = datesPedidoLab.getDate();
    this.pedidosUltrasonido.sede = CEDE;
  }

  pagar_consulta(nombre, medico){
    /* localStorage.removeItem('carrito') */
    const dates = new Dates();
    console.log(nombre);
    
    switch (nombre) {
      case '2 Ultrasonidos obstétricos convencionales':
            this.carrito={
              totalSin: 0,
              totalCon: 0,
              items: []
            };
            this.item.nombreEstudio = "ULTRASONIDO OBSTETRICO CONVENCIONAL";
            this.item._id = '5fd280031cb0ea0017f57b87';
            this.fecha = moment().format('l');    
            this.infoVenta.hora = moment().format('LT');
            this.infoVenta.vendedor = getDataStorage()._id;
            if(this.pacienteInfo.id == undefined){
              this.infoVenta.paciente = this.pacienteInfo._id;
            }else{
              this.infoVenta.paciente = this.pacienteInfo.id;
            }
            this.infoVenta.nombrePaciente = this.pacienteInfo.nombrePaciente+ ' ' + this.pacienteInfo.apellidoPaterno+ ' ' + this.pacienteInfo.apellidoMaterno
            this.infoVenta.sede = CEDE;
            this.infoVenta.prioridad = "Programado"
            this.infoVenta.fecha = dates.getDate();
            this.infoVenta.doctorQueSolicito = medico;
            /* this.carrito.items.push(this.item); */
            this.infoVenta.estudios= this.carrito.items
            this.infoVenta.totalCompra = 0;
            const ite = {
              nombreEstudio: "ULTRASONIDO OBSTETRICO CONVENCIONAL",
              precioCon:0,
              precioSin:0,
              idEstudio:'5fd280031cb0ea0017f57b87'
            }
            this.carrito.items.push(ite);
            console.log(this.infoVenta);
            
            this._pagoServicios.agregarPedido( this.infoVenta )
            .subscribe( (data) => {
              // console.log( data );
              if(  data['ok'] ){
                
                this.generarTicket(data['folio']);
                  // se crea la tabla de las ventas 
                  this.setDatesPedidos();
                  this.pedidosUltrasonido.estudios.push(ite)
                  this._pagoServicios.postPedidosUltra( this.pedidosUltrasonido ).subscribe( data => console.log( data ));
                  swal('Consulta Agregada', 'Puede ver las visitas en la tabla', 'success')
                  // ESTA INFORMCAIÓN SE ENVIA A LOS DIFERENTES SERVICIOS 
                  // seteamos las fechas 
                    eliminarStorage();
                    /* const eliminarPaciente = new PacienteStorage();
                    eliminarPaciente.removePacienteStorage();*/ 
                }
            });
            this.mostrarConsultas();
      break;
      case '1 Ultrasonido estructural':
            this.carrito={
              totalSin: 0,
              totalCon: 0,
              items: []
            };
            this.item.nombreEstudio = "ULTRASONIDO OBSTETRICO ESTRUCTURAL DEL SEGUNDO TRIMESTRE ";
            this.item._id = '5fd280c41cb0ea0017f57b91';
            this.fecha = moment().format('l');    
            this.infoVenta.hora = moment().format('LT');
            this.infoVenta.vendedor = getDataStorage()._id;
            if(this.pacienteInfo.id == undefined){
              this.infoVenta.paciente = this.pacienteInfo._id;
            }else{
              this.infoVenta.paciente = this.pacienteInfo.id;
            }
            this.infoVenta.nombrePaciente = this.pacienteInfo.nombrePaciente+ ' ' + this.pacienteInfo.apellidoPaterno+ ' ' + this.pacienteInfo.apellidoMaterno
            this.infoVenta.sede = CEDE;
            this.infoVenta.prioridad = "Programado"
            this.infoVenta.fecha = dates.getDate();
            this.infoVenta.doctorQueSolicito = medico;
            /* this.carrito.items.push(this.item); */
            this.infoVenta.estudios= this.carrito.items
            this.infoVenta.totalCompra = 0;
            const ite2 = {
              nombreEstudio: "ULTRASONIDO OBSTETRICO ESTRUCTURAL DEL SEGUNDO TRIMESTRE ",
              precioCon:0,
              precioSin:0,
              idEstudio:'5fd280c41cb0ea0017f57b91'
            }
            this.carrito.items.push(ite2);
            console.log(this.infoVenta);
            
            this._pagoServicios.agregarPedido( this.infoVenta )
            .subscribe( (data) => {
              // console.log( data );
              if(  data['ok'] ){
                
                this.generarTicket(data['folio']);
                  // se crea la tabla de las ventas 
                  this.setDatesPedidos();
                  this.pedidosUltrasonido.estudios.push(ite2)
                  this._pagoServicios.postPedidosUltra( this.pedidosUltrasonido ).subscribe( data => console.log( data ));
                  swal('Consulta Agregada', 'Puede ver las visitas en la tabla', 'success')
                  // ESTA INFORMCAIÓN SE ENVIA A LOS DIFERENTES SERVICIOS 
                  // seteamos las fechas 
                    eliminarStorage();
                    /* const eliminarPaciente = new PacienteStorage();
                    eliminarPaciente.removePacienteStorage();*/ 
                }
            });
            this.mostrarConsultas();
      break;
      case '2 Biometría hemática completa':
        this.carrito={
          totalSin: 0,
          totalCon: 0,
          items: []
        };
            this.item.nombreEstudio = "BIOMETRIA HEMATICA COMPLETA";
            this.item._id = '5fd284b11cb0ea0017f57bd8';
            this.fecha = moment().format('l');    
            this.infoVenta.hora = moment().format('LT');
            this.infoVenta.vendedor = getDataStorage()._id;
            if(this.pacienteInfo.id == undefined){
              this.infoVenta.paciente = this.pacienteInfo._id;
            }else{
              this.infoVenta.paciente = this.pacienteInfo.id;
            }
            this.infoVenta.nombrePaciente = this.pacienteInfo.nombrePaciente+ ' ' + this.pacienteInfo.apellidoPaterno+ ' ' + this.pacienteInfo.apellidoMaterno
            this.infoVenta.sede = CEDE;
            this.infoVenta.prioridad = "Programado"
            this.infoVenta.fecha = dates.getDate();
            this.infoVenta.doctorQueSolicito = medico;
            /* this.carrito.items.push(this.item); */
            this.infoVenta.estudios= this.carrito.items
            this.infoVenta.totalCompra = 0;
            const item = {
              nombreEstudio: "BIOMETRIA HEMATICA COMPLETA",
              precioCon:0,
              precioSin:0,
              idEstudio:'5fd284b11cb0ea0017f57bd8'
            }
            this.carrito.items.push(item);
            console.log(this.infoVenta);
            
            this._pagoServicios.agregarPedido( this.infoVenta )
            .subscribe( (data) => {
              // console.log( data );
              if(  data['ok'] ){
                
                this.generarTicket(data['folio']);
                  // se crea la tabla de las ventas 
                  this.setDatesPedidos();
                  this.pedidosLaboratorios.estudios.push(item);
                  console.log(this.pedidosLaboratorios);
                  
                  this._pagoServicios.pedidosLaboratorio( this.pedidosLaboratorios ).subscribe( data => console.log( data ));
                  swal('Consulta Agregada', 'Puede ver las visitas en la tabla', 'success')
                  // ESTA INFORMCAIÓN SE ENVIA A LOS DIFERENTES SERVICIOS 
                  // seteamos las fechas 
                    eliminarStorage();
                    /* const eliminarPaciente = new PacienteStorage();
                    eliminarPaciente.removePacienteStorage();*/ 
                }
            });
            this.mostrarConsultas();
      break;
      case '1 Química sanguínea de 6 elementos (Glucosa, Urea, Creatinina, Ácido Úrico, Colesterol y Triglicéidos )':
        this.carrito={
          totalSin: 0,
          totalCon: 0,
          items: []
        };
        this.item.nombreEstudio = "QUIMICA SANGUINEA 6 ";
        this.item._id = '5fd2a4171cb0ea0017f57ddb';
        this.fecha = moment().format('l');    
        this.infoVenta.hora = moment().format('LT');
        this.infoVenta.vendedor = getDataStorage()._id;
        if(this.pacienteInfo.id == undefined){
          this.infoVenta.paciente = this.pacienteInfo._id;
        }else{
          this.infoVenta.paciente = this.pacienteInfo.id;
        }
        this.infoVenta.nombrePaciente = this.pacienteInfo.nombrePaciente+ ' ' + this.pacienteInfo.apellidoPaterno+ ' ' + this.pacienteInfo.apellidoMaterno
        this.infoVenta.sede = CEDE;
        this.infoVenta.prioridad = "Programado"
        this.infoVenta.fecha = dates.getDate();
        this.infoVenta.doctorQueSolicito = medico;
        /* this.carrito.items.push(this.item); */
        this.infoVenta.estudios= this.carrito.items
        this.infoVenta.totalCompra = 0;
        const item2 = {
          nombreEstudio: "QUIMICA SANGUINEA 6",
          precioCon:0,
          precioSin:0,
          idEstudio:'5fd2a4171cb0ea0017f57ddb'
        }
        this.carrito.items.push(item2);
        this._pagoServicios.agregarPedido( this.infoVenta )
        .subscribe( (data) => {
          // console.log( data );
          if(  data['ok'] ){
            
            this.generarTicket(data['folio']);
              // se crea la tabla de las ventas
              this.setDatesPedidos();
              this.pedidosLaboratorios.estudios.push(item2);
              this._pagoServicios.pedidosLaboratorio( this.pedidosLaboratorios ).subscribe( data => console.log( data )); 
              swal('Consulta Agregada', 'Puede ver las visitas en la tabla', 'success')

              // ESTA INFORMCAIÓN SE ENVIA A LOS DIFERENTES SERVICIOS 
              // seteamos las fechas 
                eliminarStorage();
                /* const eliminarPaciente = new PacienteStorage();
                eliminarPaciente.removePacienteStorage();  */ 
          }
        });
        this.mostrarConsultas(); 
      break;
      case '2 Exámen General de Orina':
        this.carrito={
          totalSin: 0,
          totalCon: 0,
          items: []
        };
        this.item.nombreEstudio = "EXAMEN GENERAL DE ORINA ";
        this.item._id = '5fd28c2f1cb0ea0017f57c58';
        this.fecha = moment().format('l');    
        this.infoVenta.hora = moment().format('LT');
        this.infoVenta.vendedor = getDataStorage()._id;
        if(this.pacienteInfo.id == undefined){
          this.infoVenta.paciente = this.pacienteInfo._id;
        }else{
          this.infoVenta.paciente = this.pacienteInfo.id;
        }
        this.infoVenta.nombrePaciente = this.pacienteInfo.nombrePaciente+ ' ' + this.pacienteInfo.apellidoPaterno+ ' ' + this.pacienteInfo.apellidoMaterno
        this.infoVenta.sede = CEDE;
        this.infoVenta.prioridad = "Programado"
        this.infoVenta.fecha = dates.getDate();
        this.infoVenta.doctorQueSolicito = medico;
        /* this.carrito.items.push(this.item); */
        this.infoVenta.estudios= this.carrito.items
        this.infoVenta.totalCompra = 0;
        const item3 = {
          nombreEstudio: "EXAMEN GENERAL DE ORINA ",
          precioCon:0,
          precioSin:0,
          idEstudio:'5fd28c2f1cb0ea0017f57c58'
        }
        this.carrito.items.push(item3);
        this._pagoServicios.agregarPedido( this.infoVenta )
        .subscribe( (data) => {
          // console.log( data );
          if(  data['ok'] ){
            
            this.generarTicket(data['folio']);
              // se crea la tabla de las ventas 
              this.setDatesPedidos();
              this.pedidosLaboratorios.estudios.push(item3);
              this._pagoServicios.pedidosLaboratorio( this.pedidosLaboratorios ).subscribe( data => console.log( data ));
              swal('Consulta Agregada', 'Puede ver las visitas en la tabla', 'success')

              // ESTA INFORMCAIÓN SE ENVIA A LOS DIFERENTES SERVICIOS 
              // seteamos las fechas 
                eliminarStorage();
                /* const eliminarPaciente = new PacienteStorage();
                eliminarPaciente.removePacienteStorage();  */ 
          }
        });
        this.mostrarConsultas();  
      break;
      case '1 V.D.R.L':
        this.carrito={
          totalSin: 0,
          totalCon: 0,
          items: []
        };
        this.item.nombreEstudio = "V.D.R.L.";
        this.item._id = '5fd29b611cb0ea0017f57d51';
        this.fecha = moment().format('l');    
        this.infoVenta.hora = moment().format('LT');
        this.infoVenta.vendedor = getDataStorage()._id;
        if(this.pacienteInfo.id == undefined){
          this.infoVenta.paciente = this.pacienteInfo._id;
        }else{
          this.infoVenta.paciente = this.pacienteInfo.id;
        }
        this.infoVenta.nombrePaciente = this.pacienteInfo.nombrePaciente+ ' ' + this.pacienteInfo.apellidoPaterno+ ' ' + this.pacienteInfo.apellidoMaterno
        this.infoVenta.sede = CEDE;
        this.infoVenta.prioridad = "Programado"
        this.infoVenta.fecha = dates.getDate();
        this.infoVenta.doctorQueSolicito = medico;
        /* this.carrito.items.push(this.item); */
        this.infoVenta.estudios= this.carrito.items
        this.infoVenta.totalCompra = 0;
        const item4 = {
          nombreEstudio: "V.D.R.L.",
          precioCon:0,
          precioSin:0,
          idEstudio:'5fd29b611cb0ea0017f57d51'
        }
        this.carrito.items.push(item4);
        this._pagoServicios.agregarPedido( this.infoVenta )
        .subscribe( (data) => {
          // console.log( data );
          if(  data['ok'] ){
            
            this.generarTicket(data['folio']);
              // se crea la tabla de las ventas 
              this.setDatesPedidos();
              this.pedidosLaboratorios.estudios.push(item4);
              this._pagoServicios.pedidosLaboratorio( this.pedidosLaboratorios ).subscribe( data => console.log( data ));
              swal('Consulta Agregada', 'Puede ver las visitas en la tabla', 'success')

              // ESTA INFORMCAIÓN SE ENVIA A LOS DIFERENTES SERVICIOS 
              // seteamos las fechas 
                eliminarStorage();
                /* const eliminarPaciente = new PacienteStorage();
                eliminarPaciente.removePacienteStorage();   */
            }
        });
        this.mostrarConsultas();
      break;
      case '1 Grupo Sanguíneo':
        this.carrito={
          totalSin: 0,
          totalCon: 0,
          items: []
        };
        this.item.nombreEstudio = "GRUPO Y FACTOR RH";
        this.item._id = '5fd292b31cb0ea0017f57c9e';
        this.fecha = moment().format('l');    
        this.infoVenta.hora = moment().format('LT');
        this.infoVenta.vendedor = getDataStorage()._id;
        if(this.pacienteInfo.id == undefined){
          this.infoVenta.paciente = this.pacienteInfo._id;
        }else{
          this.infoVenta.paciente = this.pacienteInfo.id;
        }
        this.infoVenta.nombrePaciente = this.pacienteInfo.nombrePaciente+ ' ' + this.pacienteInfo.apellidoPaterno+ ' ' + this.pacienteInfo.apellidoMaterno
        this.infoVenta.sede = CEDE;
        this.infoVenta.prioridad = "Programado"
        this.infoVenta.fecha = dates.getDate();
        this.infoVenta.doctorQueSolicito = medico;
        /* this.carrito.items.push(this.item); */
        this.infoVenta.estudios= this.carrito.items
        this.infoVenta.totalCompra = 0;
        const item5 = {
          nombreEstudio: "GRUPO Y FACTOR RH",
          precioCon:0,
          precioSin:0,
          idEstudio:'5fd292b31cb0ea0017f57c9e'
        }
        this.carrito.items.push(item5);
        this._pagoServicios.agregarPedido( this.infoVenta )
        .subscribe( (data) => {
          // console.log( data );
          if(  data['ok'] ){
            
            this.generarTicket(data['folio']);
              // se crea la tabla de las ventas
              this.setDatesPedidos();
              this.pedidosLaboratorios.estudios.push(item5);
              this._pagoServicios.pedidosLaboratorio( this.pedidosLaboratorios ).subscribe( data => console.log( data )); 
              swal('Consulta Agregada', 'Puede ver las visitas en la tabla', 'success')

              // ESTA INFORMCAIÓN SE ENVIA A LOS DIFERENTES SERVICIOS 
              // seteamos las fechas 
                eliminarStorage();
                
                
                /* const eliminarPaciente = new PacienteStorage();
                eliminarPaciente.removePacienteStorage();   */
            }
        });
        this.mostrarConsultas();
      break;
      case '1 V.I.H.':
        this.carrito={
          totalSin: 0,
          totalCon: 0,
          items: []
        };
        this.item.nombreEstudio = "H.I.V. (PRUEBA PRESUNTIVA)";
        this.item._id = '5fd293501cb0ea0017f57caa';
        this.fecha = moment().format('l');    
        this.infoVenta.hora = moment().format('LT');
        this.infoVenta.vendedor = getDataStorage()._id;
        if(this.pacienteInfo.id == undefined){
          this.infoVenta.paciente = this.pacienteInfo._id;
        }else{
          this.infoVenta.paciente = this.pacienteInfo.id;
        }
        this.infoVenta.nombrePaciente = this.pacienteInfo.nombrePaciente+ ' ' + this.pacienteInfo.apellidoPaterno+ ' ' + this.pacienteInfo.apellidoMaterno
        this.infoVenta.sede = CEDE;
        this.infoVenta.prioridad = "Programado"
        this.infoVenta.fecha = dates.getDate();
        this.infoVenta.doctorQueSolicito = medico;
        /* this.carrito.items.push(this.item); */
        this.infoVenta.estudios= this.carrito.items
        this.infoVenta.totalCompra = 0;
        const item6 = {
          nombreEstudio: "H.I.V. (PRUEBA PRESUNTIVA)",
          precioCon:0,
          precioSin:0,
          idEstudio:'5fd293501cb0ea0017f57caa'
        }
        this.carrito.items.push(item6);
        this._pagoServicios.agregarPedido( this.infoVenta )
        .subscribe( (data) => {
          // console.log( data );
          if(  data['ok'] ){
            
            this.generarTicket(data['folio']);
              // se crea la tabla de las ventas
              this.setDatesPedidos();
              this.pedidosLaboratorios.estudios.push(item6);
              this._pagoServicios.pedidosLaboratorio( this.pedidosLaboratorios ).subscribe( data => console.log( data )); 
              swal('Consulta Agregada', 'Puede ver las visitas en la tabla', 'success')

              // ESTA INFORMCAIÓN SE ENVIA A LOS DIFERENTES SERVICIOS 
              // seteamos las fechas 
                eliminarStorage();
                
                
                /* const eliminarPaciente = new PacienteStorage();
                eliminarPaciente.removePacienteStorage(); */  
            }
        });
        this.mostrarConsultas();
      break;
      case '1 Tiempos de Coagulación':
        this.carrito={
          totalSin: 0,
          totalCon: 0,
          items: []
        };
        this.item.nombreEstudio = "TIEMPO DE COAGULACION  ";
        this.item._id = '5fd29a991cb0ea0017f57d41';
        this.fecha = moment().format('l');    
        this.infoVenta.hora = moment().format('LT');
        this.infoVenta.vendedor = getDataStorage()._id;
        if(this.pacienteInfo.id == undefined){
          this.infoVenta.paciente = this.pacienteInfo._id;
        }else{
          this.infoVenta.paciente = this.pacienteInfo.id;
        }
        this.infoVenta.nombrePaciente = this.pacienteInfo.nombrePaciente+ ' ' + this.pacienteInfo.apellidoPaterno+ ' ' + this.pacienteInfo.apellidoMaterno
        this.infoVenta.sede = CEDE;
        this.infoVenta.prioridad = "Programado"
        this.infoVenta.fecha = dates.getDate();
        this.infoVenta.doctorQueSolicito = medico;
        /* this.carrito.items.push(this.item); */
        this.infoVenta.estudios= this.carrito.items
        this.infoVenta.totalCompra = 0;
        const item7 = {
          nombreEstudio: "TIEMPO DE COAGULACION  ",
          precioCon:0,
          precioSin:0,
          idEstudio:'5fd29a991cb0ea0017f57d41'
        }
        this.carrito.items.push(item7);
        this._pagoServicios.agregarPedido( this.infoVenta )
        .subscribe( (data) => {
          // console.log( data );
          if(  data['ok'] ){
            
            this.generarTicket(data['folio']);
              // se crea la tabla de las ventas 
              this.setDatesPedidos();
              this.pedidosLaboratorios.estudios.push(item7);
              this._pagoServicios.pedidosLaboratorio( this.pedidosLaboratorios ).subscribe( data => console.log( data ));
              swal('Consulta Agregada', 'Puede ver las visitas en la tabla', 'success')

              // ESTA INFORMCAIÓN SE ENVIA A LOS DIFERENTES SERVICIOS 
              // seteamos las fechas 
                eliminarStorage();
                
                
                /* const eliminarPaciente = new PacienteStorage();
                eliminarPaciente.removePacienteStorage();   */
            }
        });
        this.mostrarConsultas();
      break;
      case '1 Curva de tolerancia a la Glucosa':
        this.carrito={
          totalSin: 0,
          totalCon: 0,
          items: []
        };
        this.item.nombreEstudio = "CURVA  DE   TOLERANCIA  A  LA  GLUCOSA  2 HRS";
        this.item._id = '5fd2a0051cb0ea0017f57d7a';
        this.fecha = moment().format('l');    
        this.infoVenta.hora = moment().format('LT');
        this.infoVenta.vendedor = getDataStorage()._id;
        if(this.pacienteInfo.id == undefined){
          this.infoVenta.paciente = this.pacienteInfo._id;
        }else{
          this.infoVenta.paciente = this.pacienteInfo.id;
        }
        this.infoVenta.nombrePaciente = this.pacienteInfo.nombrePaciente+ ' ' + this.pacienteInfo.apellidoPaterno+ ' ' + this.pacienteInfo.apellidoMaterno
        this.infoVenta.sede = CEDE;
        this.infoVenta.prioridad = "Programado"
        this.infoVenta.fecha = dates.getDate();
        this.infoVenta.doctorQueSolicito = medico;
        /* this.carrito.items.push(this.item); */
        this.infoVenta.estudios= this.carrito.items
        this.infoVenta.totalCompra = 0;
        const item8 = {
          nombreEstudio: "CURVA  DE   TOLERANCIA  A  LA  GLUCOSA  2 HRS",
          precioCon:0,
          precioSin:0,
          idEstudio:'5fd2a0051cb0ea0017f57d7a'
        }
        this.carrito.items.push(item8);
        this._pagoServicios.agregarPedido( this.infoVenta )
        .subscribe( (data) => {
          // console.log( data );
          if(  data['ok'] ){
            
            this.generarTicket(data['folio']);
              // se crea la tabla de las ventas 
              this.setDatesPedidos();
              this.pedidosLaboratorios.estudios.push(item8);
              this._pagoServicios.pedidosLaboratorio( this.pedidosLaboratorios ).subscribe( data => console.log( data ));
              swal('Consulta Agregada', 'Puede ver las visitas en la tabla', 'success')

              // ESTA INFORMCAIÓN SE ENVIA A LOS DIFERENTES SERVICIOS 
              // seteamos las fechas 
                eliminarStorage();
                
                
                /* const eliminarPaciente = new PacienteStorage();
                eliminarPaciente.removePacienteStorage();   */
            }
        });
        this.mostrarConsultas();
      break;
      case '1 Exudado Vaginal':
        this.carrito={
          totalSin: 0,
          totalCon: 0,
          items: []
        };
        this.item.nombreEstudio = "CULTIVO CERVICO VAGINAL";
        this.item._id = '5fd289731cb0ea0017f57c24';
        this.fecha = moment().format('l');    
        this.infoVenta.hora = moment().format('LT');
        this.infoVenta.vendedor = getDataStorage()._id;
        if(this.pacienteInfo.id == undefined){
          this.infoVenta.paciente = this.pacienteInfo._id;
        }else{
          this.infoVenta.paciente = this.pacienteInfo.id;
        }
        this.infoVenta.nombrePaciente = this.pacienteInfo.nombrePaciente+ ' ' + this.pacienteInfo.apellidoPaterno+ ' ' + this.pacienteInfo.apellidoMaterno
        this.infoVenta.sede = CEDE;
        this.infoVenta.prioridad = "Programado"
        this.infoVenta.fecha = dates.getDate();
        this.infoVenta.doctorQueSolicito = medico;
        /* this.carrito.items.push(this.item); */
        this.infoVenta.estudios= this.carrito.items
        this.infoVenta.totalCompra = 0;
        const item9 = {
          nombreEstudio: "CULTIVO CERVICO VAGINAL",
          precioCon:0,
          precioSin:0,
          idEstudio:'5fd289731cb0ea0017f57c24'
        }
        this.carrito.items.push(item9);
        this._pagoServicios.agregarPedido( this.infoVenta )
        .subscribe( (data) => {
          // console.log( data );
          if(  data['ok'] ){
            
            this.generarTicket(data['folio']);
              // se crea la tabla de las ventas 
              this.setDatesPedidos();
              this.pedidosLaboratorios.estudios.push(item9);
              this._pagoServicios.pedidosLaboratorio( this.pedidosLaboratorios ).subscribe( data => console.log( data ));
              swal('Consulta Agregada', 'Puede ver las visitas en la tabla', 'success')

              // ESTA INFORMCAIÓN SE ENVIA A LOS DIFERENTES SERVICIOS 
              // seteamos las fechas 
                eliminarStorage();
                
                
                /* const eliminarPaciente = new PacienteStorage();
                eliminarPaciente.removePacienteStorage();   */
            }
        });
        this.mostrarConsultas();
      break;
      case '1 Urocultivo':
        this.carrito={
          totalSin: 0,
          totalCon: 0,
          items: []
        };
        this.item.nombreEstudio = "CULTIVO DE ORINA (UROCULTIVO)";
        this.item._id = '5fd28a2b1cb0ea0017f57c32';
        this.fecha = moment().format('l');    
        this.infoVenta.hora = moment().format('LT');
        this.infoVenta.vendedor = getDataStorage()._id;
        if(this.pacienteInfo.id == undefined){
          this.infoVenta.paciente = this.pacienteInfo._id;
        }else{
          this.infoVenta.paciente = this.pacienteInfo.id;
        }
        this.infoVenta.nombrePaciente = this.pacienteInfo.nombrePaciente+ ' ' + this.pacienteInfo.apellidoPaterno+ ' ' + this.pacienteInfo.apellidoMaterno
        this.infoVenta.sede = CEDE;
        this.infoVenta.prioridad = "Programado"
        this.infoVenta.fecha = dates.getDate();
        this.infoVenta.doctorQueSolicito = medico;
        /* this.carrito.items.push(this.item); */
        this.infoVenta.estudios= this.carrito.items
        this.infoVenta.totalCompra = 0;
        const item10 = {
          nombreEstudio: "CULTIVO DE ORINA (UROCULTIVO)",
          precioCon:0,
          precioSin:0,
          idEstudio:'5fd28a2b1cb0ea0017f57c32'
        }
        this.carrito.items.push(item10);
        this._pagoServicios.agregarPedido( this.infoVenta )
        .subscribe( (data) => {
          // console.log( data );
          if(  data['ok'] ){
            
            this.generarTicket(data['folio']);
              // se crea la tabla de las ventas 
              this.setDatesPedidos();
              this.pedidosLaboratorios.estudios.push(item10);
              this._pagoServicios.pedidosLaboratorio( this.pedidosLaboratorios ).subscribe( data => console.log( data ));
              swal('Consulta Agregada', 'Puede ver las visitas en la tabla', 'success')

              // ESTA INFORMCAIÓN SE ENVIA A LOS DIFERENTES SERVICIOS 
              // seteamos las fechas 
                eliminarStorage();
                
                
                /* const eliminarPaciente = new PacienteStorage();
                eliminarPaciente.removePacienteStorage();   */
            }
        });
        this.mostrarConsultas();
      break;
      default:
        break;
    }
  }

  mostrarConsultas(){
    this.obtenerPaquete();
  }
  
  generarTicket(folio){

    const tickets = new Tickets();
    tickets.printTicketSinMembresia( this.pacienteInfo, this.carrito ,  this.infoVenta, folio );
  
  }
  mostrarConsultasDespuesParto(){
    swal( `Tipo:  ${this.consultaDespuesParto.consulta}\n  Fecha: ${this.consultaDespuesParto.fecha} \n Hora: ${this.consultaDespuesParto.hora}\n   Médico ${this.consultaDespuesParto.medico}\n  Atendió: ${this.consultaDespuesParto.firma}` );
  }

  mostrarDatos(consulta, medico, servicio){
    if(servicio == '1') swal('Citas incluidas\n','Tipo de consulta:'+consulta+'\nMedico: '+medico,'')
    if(servicio == '2') swal('Examenes de Laboratorio\n','Tipo de laboratorio:'+consulta+'\nMedico: '+medico,'')
  }

  printComponent(cmpName) { 
    let printContents = document.getElementById(cmpName).innerHTML;
    let originalContents = document.body.innerHTML;
    document.body.innerHTML = printContents;
    window.print();
    document.body.innerHTML = originalContents;
    window.location.reload();
}

}


class consultas  {

  tipo:  string;
  consulta:  string;
  fecha:  string;
  firma:  string;
  hora:  string;
  medico:  string;

}

  
class paqueteDB{ 
CitasIncluidas:  []
consultasGinecologia: Number
contenidoPaquete: []
costoTotal: 14500
examenesLaboratorio:  []
tabuladorPagos: []
icon: String;
nombrePaquete: String;
nomenclatura: String;
nomenclaturaPaciente: []
_id: String;
}