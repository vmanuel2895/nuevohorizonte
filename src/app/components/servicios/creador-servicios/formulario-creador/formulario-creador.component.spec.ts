import { ComponentFixture, TestBed } from '@angular/core/testing';

import { FormularioCreadorComponent } from './formulario-creador.component';

describe('FormularioCreadorComponent', () => {
  let component: FormularioCreadorComponent;
  let fixture: ComponentFixture<FormularioCreadorComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ FormularioCreadorComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(FormularioCreadorComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
