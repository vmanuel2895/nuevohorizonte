import { Component, Input, OnInit} from '@angular/core';
import swal from 'sweetalert';
import  Carrito  from '../../../../classes/carrito/carrito.class';
import { eliminarStorage, getCarritoStorage } from '../../../../functions/pacienteIntegrados';
import { IntegradosService } from '../../../../services/servicios/integrados.service';
import { Router, ActivatedRoute } from '@angular/router';
import { EmailService } from 'src/app/services/cotizacion/email.service';
import Swal from 'sweetalert2'
import Tickets from 'src/app/classes/tickets/ticket.class';

@Component({
  selector: 'app-tabla-service',
  templateUrl: './tabla-service.component.html',
  styleUrls: ['./tabla-service.component.css']
})
export class TablaServiceComponent implements OnInit {

  @Input() serviceSi: any [] = [];
  public carrito = {
    totalSin: 0,
    totalCon: 0,
    items: []
  };
  @Input() membresia = false;
  @Input() RoleUser = "";
  public pagina = 0;
  @Input() servicios ="";
  @Input() totalAmbulancia: string;
  public show = 'hidden';
  public btnPago = false;
  public btnImpPago = false;
  public email="";
  public sedePaciente = '';
  public dataSedePaciente = {
    idSede:'', 
    name:'', 
    idItem:''
  }
  public idPedidoSede;

  constructor(private _service:IntegradosService, 
              private _router:Router,
              private _emailService: EmailService,){}

  ngOnInit(): void {
    this.obtenerCarritoStorage(); 
    // console.log(this.servicios);
    this.obtenerPacienteYMembresia(); 
    this.obtenerSedePaciente();  
  }

  obtenerSedePaciente(){
    this.sedePaciente= localStorage.getItem('sedePaciente');
    if(this.sedePaciente == ''){
      this.sedePaciente = ''
    }
  }

  obtenerPacienteYMembresia(){
    // se obtienen los precios con membresia 
    let usuario = JSON.parse( localStorage.getItem('paciente'));
    /* console.log( usuario ); */  
    if( usuario != null ){
      this.btnPago = true;
      this.btnImpPago = false; 
      this.verDatos();      
      this.membresia = usuario.membresiaActiva;
    }else {
      this.btnPago = false;
      this.btnImpPago = true; 
      this.verDatos(); 
    }
    /* if(this.idPedidoSede == null){
      this.btnPago = false;
      this.btnPagoSede = false;
      this.btnImpPago = true;
    }else{
      this.btnPago = false;
      this.btnPagoSede = true;
      this.btnImpPago = false;
    }  */
    /* console.log( this.btnPago ); */
  }
  
  onClick(){
    this.idPedidoSede = localStorage.getItem('idPedidoSede');
    if(this.idPedidoSede == null){
      this._router.navigateByUrl('/pago/servicios');
    }else{
      this._router.navigateByUrl('/pedidos/detalle/'+this.idPedidoSede);
    }
  }


  obtenerCarrito (){
    this.carrito = getCarritoStorage();
    if ( this.carrito == null ){
        this.carrito = {
          totalSin: 0,
          totalCon: 0,
          items: []
        };
    }
  }

  obtenerCarritoStorage(){
    const storageCarrito = new Carrito();
    this.carrito = storageCarrito.obtenerSotorageCarrito();
  }
  cerrarModal(){
    this.show = 'hidden'
  }

  abrirInputCorreo(){
    this.show = 'show';
  }

  enviar( ){
    let cotizacion ={
      correo: this.email,
      carrito: this.carrito
    }
    this._emailService.envioEmail( cotizacion ).subscribe( (data:any) => {
      if(data.ok){
        swal('cotización enviada','se envio exitosamente', 'success');
        this.show = 'hidden';
      }
    })
  }

  imprimirCotizacion(carrito){
    this.generarTicket(carrito);
  }

  generarTicket(carrito){
    const tickets = new Tickets();
    tickets.imprimirCotizacion(carrito);
    eliminarStorage();
    this.obtenerCarrito();
  }

  agregarCarrito( event, item: any ) {
    // console.log(event);
    let carrito = new Carrito();
    /* swal("Con membrecia ahorras:",{icon:"success"}); */
    this.carrito = carrito.agregarItem(  event, item );
    this.validarCarrito();
  }

  agregarCarritoSede( event, item: any, uti, rango ) {
    // console.log(event);
    let carrito = new Carrito();
    /* swal("Con membrecia ahorras:",{icon:"success"}); */
    this.carrito = carrito.agregarItemUtilidad(  event, item,uti,rango );
    this.validarCarrito();
  }

  eliminar(index){
    //console.log('Pedido', pedido);
    let carrito = new Carrito();
    carrito.eliminar( index );
    this.obtenerCarritoStorage();
    this.validarCarrito();
  }

  validarCarrito(){
    this.obtenerCarritoStorage();
    if(this.carrito.items.length == 0){
      return true;
    }else{
      return false;
    }
  }
  
  async alertcomparasion( ev, precioPublico, precioMembresia, item2:any ){
    let precioSinTrim  =  precioPublico.replace('$', '');
    let precioSinComaPublico = precioSinTrim.replace(',', '');
    let precioMemTrim  =  precioMembresia.replace('$', '');
    let precioMemComaMembresia = precioMemTrim.replace(',', '');
    console.log(this.sedePaciente);
    
    if(this.sedePaciente === null){
      this.agregarCarrito(ev,item2);
    }else{
      this._service.getIdSedeNom(this.sedePaciente).subscribe((data:any)=>{
        if(data.ok){
          this.dataSedePaciente.idSede = data['data'][0]._id;
          this.dataSedePaciente.name = this.servicios;
          this.dataSedePaciente.idItem = item2._id;
          this._service.getItemSede(this.dataSedePaciente).subscribe((data:any)=>{
            console.log
            if(data.ok){  
              if(data['data'].length == 0){
                this.agregarCarrito(ev,item2); 
              }else{
                  let uti= '';
                  switch(data['data'][0].rangoUtilidad){
                    case 'A':
                      if(this.membresia){
                        uti=data['data'][0].preciosRangoA[0].porcentajePrecioPublicoA
                      }else{
                        uti=data['data'][0].preciosRangoA[0].porcentajePrecioMembresiaA
                      }
                    break;
                    case 'B':
                      if(this.membresia){
                        uti=data['data'][0].preciosRangoB[0].porcentajePrecioPublicoB
                      }else{
                        uti=data['data'][0].preciosRangoB[0].porcentajePrecioMembresiaB
                      }
                    break;
                    case 'C':
                      if(this.membresia){
                        uti=data['data'][0].preciosRangoC[0].porcentajePrecioPublicoC
                      }else{
                        uti=data['data'][0].preciosRangoC[0].porcentajePrecioMembresiaC
                      }
                    break;
                  }
                this.agregarCarritoSede(ev,data['data'][0].idServicio, uti, data['data'][0].rangoUtilidad)              
              }
              
            }
          })
          
        }
      })
      /* this._service.getItemSede(this.dataSedePaciente).subscribe((data:any)=>{
        if(data.ok){
          console.log(data);
          
        }
      }) */
    }
    /* this.agregarCarrito(ev, item2) */
    if(this.servicios === 'xray'){
      this.interpretacion(ev,precioSinComaPublico,precioMemComaMembresia,item2);
    }else{
      swal("Con membrecia ahorras:"+(precioSinComaPublico - precioMemComaMembresia),{icon:"success"})
    }
  }

  async interpretacion(ev,precioSinComaPublico,precioMemComaMembresia, item2){
    const { value: accept } = await Swal.fire({
      title: 'Con membrecia ahorras: $'+(precioSinComaPublico - precioMemComaMembresia),
      input: 'checkbox',
      inputValue: 1,
      icon:"success",
      inputPlaceholder:
        'Agregar interpretacion, recuerda que tiene un costo extra',
      confirmButtonText:
        'Aceptar',
    });
  
    if (accept) {
      const item = {
        name: "xray",
        ESTUDIO: "INTERPRETACIÓN DE "+item2.ESTUDIO,
        PRECIO_PUBLICO:"$100",
        PRECIO_MEMBRESIA:"$100",
        _id: "0"
      }  
      this.agregarCarrito(ev,item);
    }
  }

  verDatos(){
    this._service.getObtener(this.servicios).subscribe(
      (res: any) => {
          this.serviceSi = res.data;
          console.log(res);
          
          this.totalAmbulancia = res.data.results;
      },
      err => {
        console.log(<any>err);
    });   
  }

  showAlert(){
    swal({title: "Estas seguro de contratar a este destino?",
    text: "El servicio de ambulancia solo será requerido para dicho destino, no puede haber cambios",
    icon: "warning",
    buttons: {
      cancel: {
        text: "Cancelar",
        value: null,
        visible: true,
        className: "",
        closeModal: true,
      },
      confirm: {
        text: "OK",
        value: true,
        visible: true,
        className: "",
        closeModal: true
      }
    },
    dangerMode: true,
  })
  .then((value) => {
    console.log( value );
    if (value) {
      swal("Vamos a llenar el papeleo!", {
        icon: "success",
      });
      this._router.navigateByUrl('/hoja-fram');
    } else if( value == null ) {
      swal("Tranquilo, Puedes intentar contratar algun otro destino!", {
        icon: "error",
      });
    }});

  }

  editarServicio(id){
    swal({title: "Estas seguro de Editar este servicio?",
    text: "Una vez que se haya editado el servicio, no se podrá recuperar",
    icon: "warning",
    buttons: [true, true],
    dangerMode: true,
  })
  .then((willDelete) => {
    if (willDelete) {
      this._router.navigateByUrl('formularioeditarservice/' +id)
    } else if (willDelete == null){
      swal("Tranquilo, el servicio sigue estando ahí..", {
        icon: "error",
      });
      this._router.navigateByUrl('serviciosInt/endoscopia');
    }});
  }

  delete(id) {
    swal(
      {title:"¿Estas seguro de eliminar este servicio?",text:"Una vez eliminado el servicio, no se podra recuperar",icon:"warning",
      buttons: {
        cancel: {
          text: "Cancelar",
          value: null,
          visible: true,
          className: "",
          closeModal: true,
        },
        confirm: {
          text: "OK",
          value: true,
          visible: true,
          className: "",
          closeModal: true
        }
      },
      dangerMode: true,}
    ).then((willDelete) => {
      if (willDelete) {
        this._service.deleteService(id).subscribe(
          response => {
            this.verDatos();
            this._router.navigateByUrl('serviciosInt/endoscopia');
            swal("El servicio se ha eliminado correctamente",{icon:"success"});
          },
          error => {
            swal(error);
          }
        );
      } else if (willDelete == null){
        swal("Tranquilo, el destino sigue estando ahí..", {
          icon: "error",
        });
      }}); 
  }

}
