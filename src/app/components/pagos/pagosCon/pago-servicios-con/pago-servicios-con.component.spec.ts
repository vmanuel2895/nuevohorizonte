import { ComponentFixture, TestBed } from "@angular/core/testing";
import Carrito from "src/app/classes/carrito/carrito.class";
import { PagoServiciosService } from "src/app/services/pagos/pago-servicios.service";
import { PagoServiciosConComponent  } from './pago-servicios-con.component';

describe('Pruebas con el componente de PagoServiciosConComponent', () => {
    
    let componente: PagoServiciosConComponent;
    let fixture: ComponentFixture<PagoServiciosConComponent>;

    beforeEach( ()=> {
        // componente = new PagoServiciosConComponent()
        TestBed.configureTestingModule({
            declarations:[PagoServiciosConComponent ],
            providers:[PagoServiciosService]
        });

        // ya esta compilado el componente 
        fixture =  TestBed.createComponent( PagoServiciosConComponent );
        componente = fixture.componentInstance;
    });


    test('Debe de retornar el carrito vacio ', () => {

        const returnCarro =  { totalSin: 0, totalCon: 0, items: [] }

        const carro = new Carrito();
        const storageCarro = carro.obtenerSotorageCarrito();
        // console.log(storageCarro);

        expect( storageCarro ).toEqual( returnCarro );

    });


    test('Debe de crearse el componente de forma correcta', ()=> {

        expect( componente ).toBeTruthy();
    
    });

    // test('Debe de sacar el IVA de 100', ()=> {
       
    //     const iva = 16;
    
        
    //     expect( componente.calcularIva( iva ) ).toBe( iva );        
    // });

});
