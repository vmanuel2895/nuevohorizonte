import { ComponentFixture, TestBed } from '@angular/core/testing';

import { UtilidadSocioAmbulanciaComponent } from './utilidad-socio-ambulancia.component';

describe('UtilidadSocioAmbulanciaComponent', () => {
  let component: UtilidadSocioAmbulanciaComponent;
  let fixture: ComponentFixture<UtilidadSocioAmbulanciaComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ UtilidadSocioAmbulanciaComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(UtilidadSocioAmbulanciaComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
