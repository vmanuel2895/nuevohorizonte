import { ComponentFixture, TestBed } from '@angular/core/testing';

import { UtilidadSocioServiciosComponent } from './utilidad-socio-servicios.component';

describe('UtilidadSocioServiciosComponent', () => {
  let component: UtilidadSocioServiciosComponent;
  let fixture: ComponentFixture<UtilidadSocioServiciosComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ UtilidadSocioServiciosComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(UtilidadSocioServiciosComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
