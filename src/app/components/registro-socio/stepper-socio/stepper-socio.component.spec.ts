import { ComponentFixture, TestBed } from '@angular/core/testing';

import { StepperSocioComponent } from './stepper-socio.component';

describe('StepperSocioComponent', () => {
  let component: StepperSocioComponent;
  let fixture: ComponentFixture<StepperSocioComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ StepperSocioComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(StepperSocioComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
