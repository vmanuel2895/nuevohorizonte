import { ComponentFixture, TestBed } from '@angular/core/testing';

import { StepperSocioEditarAmbulanciaComponent } from './stepper-socio-editar-ambulancia.component';

describe('StepperSocioEditarAmbulanciaComponent', () => {
  let component: StepperSocioEditarAmbulanciaComponent;
  let fixture: ComponentFixture<StepperSocioEditarAmbulanciaComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ StepperSocioEditarAmbulanciaComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(StepperSocioEditarAmbulanciaComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
