import { ComponentFixture, TestBed } from '@angular/core/testing';

import { StepperSocioEditarComponent } from './stepper-socio-editar.component';

describe('StepperSocioEditarComponent', () => {
  let component: StepperSocioEditarComponent;
  let fixture: ComponentFixture<StepperSocioEditarComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ StepperSocioEditarComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(StepperSocioEditarComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
