import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
  name: 'buscarPaciente'
})
export class BuscarPacientePipe implements PipeTransform {

  transform(value: any, args: any): any {
    const buscarPaciente = [];
    for ( const paciente of value) {
      if(paciente.nombrePaciente.toLowerCase().indexOf(args.toLowerCase()) > - 1){
        console.log('Encontrado');
        
        buscarPaciente.push(paciente);

      }
      
    }
    return buscarPaciente;
  }


}
