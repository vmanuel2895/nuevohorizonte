import { Injectable } from '@angular/core';
import { CanActivate, ActivatedRouteSnapshot, RouterStateSnapshot, UrlTree } from '@angular/router';
import {   getDataStorage } from '../../functions/storage.funcion'
import { Observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class RecpecionRolGuard implements CanActivate {
  canActivate(
    next: ActivatedRouteSnapshot,
    state: RouterStateSnapshot): Observable<boolean | UrlTree> | Promise<boolean | UrlTree> | boolean | UrlTree {


      /* let usuarioRole = getDataStorage().role;

      if( usuarioRole == 'Recepcion' || usuarioRole == 'Admin' ){

        return true;
      }
      return false; */

      try {
        let usuarioRole = getDataStorage().role;
        if( usuarioRole == 'Recepcion' || usuarioRole == 'Admin' ){
  
          return true;
        }else{
          return false;
        } 
      } catch (error) {
        return true;
      }

  }

}
