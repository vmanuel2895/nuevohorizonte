import { TestBed } from '@angular/core/testing';

import { FarmaciaRolGuard } from './farmacia-rol.guard';

describe('FarmaciaRolGuard', () => {
  let guard: FarmaciaRolGuard;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    guard = TestBed.inject(FarmaciaRolGuard);
  });

  it('should be created', () => {
    expect(guard).toBeTruthy();
  });
});
