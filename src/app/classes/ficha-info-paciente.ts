export interface FichaInfo {
    nombrePaciente: String;
    apellidoPaterno: String;
    apellidoMaterno: String;
    curp: String;
    edad: Number;
    genero: String;
    _id:String;
    callePaciente: String;
    fechaNacimientoPaciente:String;
    estadoPaciente: String;
    paisPaciente: String;
    telefono: String;

} 