export interface Pedidos{
    doctorQueSolicito: String;
    efectivo: String;
    estudios: [];
    fecha: String;
    hora: String;
    montoEfectivo:  Number;
    montoTarjetaDebito:  Number;
    montoTarjteCredito:  Number;
    montoTranferencia:  Number;
    paciente: String;
    sede: String;
    tarjetCredito: String;
    tarjetaDebito: String;
    totalCompra: Number;
    transferencia: String;
    vendedor: String;
    ventasOrigen:  String;    
    _id: String;
}