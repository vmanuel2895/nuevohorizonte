import { ComponentFixture, TestBed } from '@angular/core/testing';

import { ResultadosRayosxComponent } from './resultados-rayosx.component';

describe('ResultadosRayosxComponent', () => {
  let component: ResultadosRayosxComponent;
  let fixture: ComponentFixture<ResultadosRayosxComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ ResultadosRayosxComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(ResultadosRayosxComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
