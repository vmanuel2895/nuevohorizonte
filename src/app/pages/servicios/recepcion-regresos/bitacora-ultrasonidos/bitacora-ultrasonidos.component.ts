import { Component, OnInit } from '@angular/core';
import { USGService } from 'src/app/services/usg/usg.service';

@Component({
  selector: 'app-bitacora-ultrasonidos',
  templateUrl: './bitacora-ultrasonidos.component.html',
  styleUrls: ['./bitacora-ultrasonidos.component.css']
})
export class BitacoraUltrasonidosComponent implements OnInit {
  public consultas:any=[]
  public pagina = 0;
  public totalAmbulancia: string;

  constructor(  public _usgservicio : USGService) { }

  ngOnInit(): void {
    this.obtenerCosnultaUltra()
  }

  
  obtenerCosnultaUltra(){
    this._usgservicio.getUltrsonidoUSG()
    .subscribe( (data) =>   {
      
        this.consultas = data['data'].reverse();
        this.totalAmbulancia = data['data'].results;
        /* console.log(this.consultas); */
        
  
    });
  }
}
