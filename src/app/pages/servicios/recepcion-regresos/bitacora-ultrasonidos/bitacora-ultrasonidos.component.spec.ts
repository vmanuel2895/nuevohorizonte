import { ComponentFixture, TestBed } from '@angular/core/testing';

import { BitacoraUltrasonidosComponent } from './bitacora-ultrasonidos.component';

describe('BitacoraUltrasonidosComponent', () => {
  let component: BitacoraUltrasonidosComponent;
  let fixture: ComponentFixture<BitacoraUltrasonidosComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ BitacoraUltrasonidosComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(BitacoraUltrasonidosComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
