import { ComponentFixture, TestBed } from '@angular/core/testing';

import { BitacoraRayosxComponent } from './bitacora-rayosx.component';

describe('BitacoraRayosxComponent', () => {
  let component: BitacoraRayosxComponent;
  let fixture: ComponentFixture<BitacoraRayosxComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ BitacoraRayosxComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(BitacoraRayosxComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
