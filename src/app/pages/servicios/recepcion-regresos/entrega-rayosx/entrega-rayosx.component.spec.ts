import { ComponentFixture, TestBed } from '@angular/core/testing';

import { EntregaRayosxComponent } from './entrega-rayosx.component';

describe('EntregaRayosxComponent', () => {
  let component: EntregaRayosxComponent;
  let fixture: ComponentFixture<EntregaRayosxComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ EntregaRayosxComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(EntregaRayosxComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
