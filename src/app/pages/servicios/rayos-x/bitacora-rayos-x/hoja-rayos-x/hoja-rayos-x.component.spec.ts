import { ComponentFixture, TestBed } from '@angular/core/testing';

import { HojaRayosXComponent } from './hoja-rayos-x.component';

describe('HojaRayosXComponent', () => {
  let component: HojaRayosXComponent;
  let fixture: ComponentFixture<HojaRayosXComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ HojaRayosXComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(HojaRayosXComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
