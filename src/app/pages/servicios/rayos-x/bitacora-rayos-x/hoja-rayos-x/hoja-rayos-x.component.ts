import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { XRAYService } from 'src/app/services/Rayos-X/xray.service';

@Component({
  selector: 'app-hoja-rayos-x',
  templateUrl: './hoja-rayos-x.component.html',
  styleUrls: ['./hoja-rayos-x.component.css']
})
export class HojaRayosXComponent implements OnInit {


  public paciente  = {
    nombre: '',
    apellidoPaterno: '',
    apellidoMaterno: '',
    estadoPaciente: '',
    fechaNacimiento: '',
    telefono: '',
    edad: 0,
    genero: '',
    curp:'',
    callePaciente:'',
    cpPaciente:'',
    paisPaciente:'',
    idMedicinaPreventiva: '',
    idAntecedentesHeredoFam: '',
    idPaciente:''
  };

  public estudios={
    estudios: [{}],
    fechaDePedidoDeLosExamenes: "",
    idPaciente: "",
    prioridad: "",
    sede: "",
    _id :""
  }

public id: string
   
  constructor(
    private activatedRoute: ActivatedRoute,
    private _xrayService : XRAYService
  ) { }

  ngOnInit( ): void {
    this.id = this.activatedRoute.snapshot.paramMap.get('id')
    this.obtenerXray();
   }

   grabarestudioXray()
  {
    localStorage.setItem('xray',JSON.stringify(this.id));
    localStorage.setItem('fechaXray',JSON.stringify(this.estudios.fechaDePedidoDeLosExamenes));
  }

  grabaridPaciente(){
    localStorage.setItem('idPaciente',JSON.stringify(this.estudios.idPaciente));
    localStorage.setItem('idPedidoXray',JSON.stringify(this.id));
  }


   datosPaciente(paciente ){
    this.paciente=paciente
    console.log(this.paciente)
  }

  obtenerXray(){
    this._xrayService.verPedidoXray(this.id)
    .subscribe((data) => {
      
      this.estudios = data['data']
      console.log(this.estudios);
      
      this.datosPaciente(data['data']['idPaciente'])

       this.grabaridPaciente()
    })  

  }
}
