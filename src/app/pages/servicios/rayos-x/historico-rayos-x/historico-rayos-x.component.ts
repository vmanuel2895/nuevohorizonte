import { Component, OnInit } from '@angular/core';
import { IntegradosService } from 'src/app/services/servicios/integrados.service';

@Component({
  selector: 'app-historico-rayos-x',
  templateUrl: './historico-rayos-x.component.html',
  styleUrls: ['./historico-rayos-x.component.css']
})
export class HistoricoRayosXComponent implements OnInit {


  public Xary

  constructor(     private _consumoServicioXray: IntegradosService) { }

  ngOnInit(): void {
    this.obtenerDataXray();
  }
  obtenerDataXray(){
    this._consumoServicioXray.getObtener('xray')
    .subscribe((data:any) => {
      this.Xary = data['data'];
      console.log( data);
      
    })
  }
}
