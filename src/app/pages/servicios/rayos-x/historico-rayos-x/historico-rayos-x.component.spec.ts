import { ComponentFixture, TestBed } from '@angular/core/testing';

import { HistoricoRayosXComponent } from './historico-rayos-x.component';

describe('HistoricoRayosXComponent', () => {
  let component: HistoricoRayosXComponent;
  let fixture: ComponentFixture<HistoricoRayosXComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ HistoricoRayosXComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(HistoricoRayosXComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
