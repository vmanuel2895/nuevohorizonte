import { ComponentFixture, TestBed } from '@angular/core/testing';

import { FormularioRayosXComponent } from './formulario-rayos-x.component';

describe('FormularioRayosXComponent', () => {
  let component: FormularioRayosXComponent;
  let fixture: ComponentFixture<FormularioRayosXComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ FormularioRayosXComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(FormularioRayosXComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

/*Irving Alejandro Andrade Gonzalez
  Anahi Lopez Sanchez

  Alicia Sanchez Sanchez

  5 Años Rel

  Sofia Andrade Lopez
  3 Años*/