import { Component, OnInit } from '@angular/core';
import { FormGroup, FormBuilder, FormArray } from '@angular/forms';
import { ActivatedRoute,Router } from '@angular/router';
import { IntegradosService } from 'src/app/services/servicios/integrados.service';
import swal from 'sweetalert/dist/sweetalert.min.js';

@Component({
  selector: 'app-formulario-rayos-x',
  templateUrl: './formulario-rayos-x.component.html',
  styleUrls: ['./formulario-rayos-x.component.css']
})
export class FormularioRayosXComponent implements OnInit {

  forma: FormGroup;
  public id;
  public elementos = {
    ELEMENTOS: '',

    ESTUDIO: ''
  };
  constructor(private fb: FormBuilder,private _routes : Router, private _route: ActivatedRoute , private _routeservi : IntegradosService) { 
    this.crearFormulario();
  }

  ngOnInit(): void {
    this.id = this._route.snapshot.paramMap.get('id');
    this.obtenerXray();
  }

  obtenerXray(){
    this._routeservi.getServicioById(this.id)
    .subscribe((data:any) => {
      this.elementos = data['data'];
    })

  }
  get grupos(){
    return this.forma.get('grupos') as FormArray;
  }


  crearFormulario(){
    this.forma = this.fb.group
    ({
     grupos:this.fb.array([]),
    });
   
   this.agregarCampo()
   
     }//fin metodo crearformulario()


     
  agregarCampo(){
    const elemento =  this.fb.group({
         machote: '',
       });
       this.grupos.push(elemento)
     }
     borrarCampo(i :  number){
       this.grupos.removeAt(i);
     }
   
       vista(){
         console.log(this.forma)
         
       }


       enviarXray( ) {
        this._routeservi.actualizarElemtos(this.id, this.forma.value)
        .subscribe((data:any) => {
          this.elementos = data['data'];
          
          /* alert('Se ha enviado interpretación de Rayos x')
          console.log(data); */
          swal ( 
            "DATOS ENVIADOS",
             "Información enviada correctamente", 
            "success",{ button:"Entendido"}
            
           )
          
        })
        this._routes.navigate(['/historico/rayos/x'])
      }

}
