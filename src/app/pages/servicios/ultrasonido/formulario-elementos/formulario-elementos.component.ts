import { Component, OnInit } from '@angular/core';
import { FormGroup, FormBuilder, FormArray } from '@angular/forms';
import { ActivatedRoute } from '@angular/router';
import { IntegradosService } from '../../../../services/servicios/integrados.service';
import swal from 'sweetalert/dist/sweetalert.min.js';
import { log } from 'console';
@Component({
  selector: 'app-formulario-elementos',
  templateUrl: './formulario-elementos.component.html',
  styleUrls: ['./formulario-elementos.component.css']
})
export class FormularioElementosComponent implements OnInit {

  

  forma: FormGroup;
  public id;
  public elementos = {
    ELEMENTOS: '',

    ESTUDIO: ''
  };

  constructor(private fb: FormBuilder,private _route: ActivatedRoute , private _routeservi : IntegradosService) { 
    this.crearFormulario();
  }

  ngOnInit(): void {
    this.id = this._route.snapshot.paramMap.get('id');
    this.obtenerUSG();
  }

  obtenerUSG(){
    this._routeservi.getServicioById(this.id)
    .subscribe((data:any) => {
      this.elementos = data['data'];
    })
 console.log(this.elementos)
  }


  get grupos(){
    return this.forma.get('grupos') as FormArray;
  }


  crearFormulario(){
    this.forma = this.fb.group
    ({
     grupos:this.fb.array([]),
    });
   
   this.agregarCampo()
   
     }//fin metodo crearformulario()


     
  agregarCampo(){
    const elemento =  this.fb.group({
         machote: '',
       });
       this.grupos.push(elemento)
     }
     borrarCampo(i :  number){
       this.grupos.removeAt(i);
     }
   
       vista(){
         console.log(this.forma)
         
       }

  enviarUSG( ) {
    this._routeservi.actualizarElemtos(this.id, this.forma.value)
    .subscribe((data:any) => {
      this.elementos = data['data'];
      /* alert('Se ha enviado MACHOTE de Estudio')
      console.log(data); */
      swal ( 
        "DATOS ENVIADOS",
         "Información enviada correctamente", 
        "success",{ button:"Entendido"}
        
       )
      
    })
  }
}
