import { Component, OnInit } from '@angular/core';
import { IntegradosService } from '../../../../services/servicios/integrados.service';

@Component({
  selector: 'app-historico-ultrasonido',
  templateUrl: './historico-ultrasonido.component.html',
  styleUrls: ['./historico-ultrasonido.component.css']
})

export class HistoricoUltrasonidoComponent implements OnInit {

  public usg;

  constructor(
    private _consumoServicioUSG: IntegradosService,
  ) { }

  ngOnInit(): void {
    this.obtenerDataUsg();
  }

  
  obtenerDataUsg(){
    this._consumoServicioUSG.getObtener('ultrasonido')
    .subscribe((data:any) => {
      this.usg = data['data'];
      console.log("Aquí estan tus putos usg", data);
      
    })
  }

}
