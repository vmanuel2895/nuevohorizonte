import { Component, OnInit } from '@angular/core';

import { IntegradosService } from '../../../../services/servicios/integrados.service';


@Component({
  selector: 'app-historico-estudios',
  templateUrl: './historico-estudios.component.html',
  styleUrls: ['./historico-estudios.component.css']
})
export class HistoricoEstudiosComponent implements OnInit {

  public tablaEstudios ;
  public totalAmbulancia: string;
  public pagina = 0;
  constructor( private consumoServicio : IntegradosService) { }

  ngOnInit(): void {
  
    this.obtnerDatos();
  }


  obtnerDatos(){
this.consumoServicio.getObtener('laboratorio')
.subscribe( (data:any) => {
  if(data.ok){
    this.tablaEstudios =data ['data'] 
    this.totalAmbulancia = this.tablaEstudios.results;
  }
// console.log(data)
})
}





}
