import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { getDataStorage } from 'src/app/functions/storage.funcion';
import { ConsultaService } from 'src/app/services/consultas/consulta/consulta.service';
import { LaboratorioService } from 'src/app/services/consultasLab/laboratorio.service';
import { IntegradosService } from 'src/app/services/servicios/integrados.service';
import swal from 'sweetalert/dist/sweetalert.min.js';

import   {jsPDF}    from  "jspdf" ;
import { NgForm } from '@angular/forms';

@Component({
  selector: 'app-hoja-reporte',
  templateUrl: './hoja-reporte.component.html',
  styleUrls: ['./hoja-reporte.component.css']
})
export class HojaReporteComponent implements OnInit {


public nombre :string
  public id_: string


public datosObtenido={
  idPaciente:"",
  idPedido:"",
  idEstudio:"",
  obtenidos:[],
  quimico:""
}

public obtenido={

  resultados:[],

}

public pedido  = {
ESTUDIO:"",
ELEMENTOS: [{ referencia:[]}], 
  _id: "", 
  idPaciente:"", 
  fecha: "", 
  hora: ""
};

  public paciente  = {
    nombre: '',
    apellidoPaterno: '',
    apellidoMaterno: '',
    estadoPaciente: '',
    fechaNacimiento: '',
    telefono: '',
    edad: 0,
    genero: '',
    curp:'',
    callePaciente:'',
    cpPaciente:'',
    paisPaciente:'',
    idMedicinaPreventiva: '',
    idAntecedentesHeredoFam: '',
    idPaciente:'',
    _id:""
  };

  public idestudiosAll : [{
    idEstudio:""
  }]

  public estadoE={ 
         estado :"finalizado"}
  public usuario={nombre:""};
  public estudio:any
  public activar = false;
  public ref=[];
  public quimico;
  public codPedido:""

  constructor( private _routers: Router, private _integradosServicios : IntegradosService,
     public _consultaService: ConsultaService,private activatedRoute: ActivatedRoute,  private _laboratorio:  LaboratorioService) { }

  ngOnInit(): void {

     this.id_ = this.activatedRoute.snapshot.paramMap.get('id');
 console.log(this.id_);
 
   this.usuario= getDataStorage();
 this.quimico = this.usuario.nombre;
  
   this.obtenerLocalStorage()
   this.obtenerEstudios()
   

  
  }


  obtenerLocalStorage(){
    this.estudio=JSON.parse(localStorage.getItem('estudio'))
   this.pedido=JSON.parse(localStorage.getItem('idPedido'))
   this.codPedido= JSON.parse(localStorage.getItem('idPedido'))
    this.paciente = JSON.parse(localStorage.getItem('idPaciente'));
    this.idestudiosAll = JSON.parse (localStorage.getItem('idEstudioU'))
//  console.log(this.idestudiosAll)

   

  }


  validarSe(){
    if(this.estadoE.estado !=""){
      
      this.activar = true
        
    }
   
    
  }

  validarSelect(){
  if (this.estadoE.estado ==""){
    swal ( 
      "SELECCIONE ESTADO",
       "Información incompleta", 
      "warning",{ button:"Entendido"}
      
     )
     this.activar = false
      }
     
      else if (this.estadoE.estado != "") {
        this.activar = true
        
      }

  }



  obtenerEstudios(){

    this._integradosServicios.buscarServicio( this.id_ )
    .subscribe((data) =>    this.setPedido(  data['data']  ))
  }

 setPedido(  pedido ){
  this.pedido = pedido;
  console.log(pedido )
 }  
  
 enviarObtenido (form: NgForm) {
  /* console.log(form);
    if(form.invalid){
      Object.values(form.controls).forEach(element =>{
        if(element.status === 'INVALID'){

        }
      });
    }
    */
   this.datosObtenido.idPaciente= this.paciente._id;
   this.datosObtenido.idEstudio = this.id_
   this.datosObtenido.idPedido =this.codPedido
   this.datosObtenido.quimico = this.quimico
  //  console.log(this.datosObtenido)
  

this.datosObtenido.obtenidos
this.datosObtenido.obtenidos.push(form.value)


/* this.updateEstado(); */
  this._laboratorio.enviarObtenidosLab(this.datosObtenido)
  .subscribe((data :any)=>{
    if(data.ok){
      this.updateEstado();
      swal ( 
        "DATOS ENVIADOS",
        "Información enviada correctamente", 
        "success",{ button:"Entendido"}
      )
      this.eliminarLocal();
    }
    
  })
  
}

eliminarLocal(){
  localStorage.removeItem('idEstudioU');
  localStorage.removeItem('idPedidoUltra');
  this._routers.navigate(['/hoja-servicios/'+ this.estudio])
 }


 generarPdf(){
   const doc = new jsPDF();
   doc.html('form');
   doc.save('Resultado de Estudios');
 }



  updateEstado(){
    const idPedido= JSON.parse(localStorage.getItem('idPedido'));
    /* console.log(this.estadoE); */
    
    this._laboratorio.actualizarEstado(idPedido,this.id_,this.estadoE).subscribe((data:any) =>{
      if(data.ok){
        console.log(data);
        
      }
    });
  }

}
