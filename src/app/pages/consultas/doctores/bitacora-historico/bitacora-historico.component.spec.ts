import { ComponentFixture, TestBed } from '@angular/core/testing';

import { BitacoraHistoricoComponent } from './bitacora-historico.component';

describe('BitacoraHistoricoComponent', () => {
  let component: BitacoraHistoricoComponent;
  let fixture: ComponentFixture<BitacoraHistoricoComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ BitacoraHistoricoComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(BitacoraHistoricoComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
