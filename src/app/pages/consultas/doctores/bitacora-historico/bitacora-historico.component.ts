import { Component, OnInit } from '@angular/core';
import { ConsultaService } from 'src/app/services/consultas/consulta/consulta.service';
import { PacientesService  } from '../../../../services/pacientes/pacientes.service';
import jsPDF from 'jspdf';
import 'jspdf-autotable';

@Component({
  selector: 'app-bitacora-historico',
  templateUrl: './bitacora-historico.component.html',
  styleUrls: ['./bitacora-historico.component.css']
})
export class BitacoraHistoricoComponent implements OnInit {
  public consultas:any [] = [];
  public totalAmbulancia: string;
  public noSe=[];
  public pagina = 0;
  public imprimir = {
    indice: 0,
    fecha: '',
    nombre:  '',
    genero: '',
    edad: '',
    medico: '',
  }
  public listaEspera = [];
  public getPacienteSotageX = this.listaEspera;
  public filterPost = ''

  constructor(     
    private _consultasService: ConsultaService,
    ) { }

  ngOnInit(): void {
    this.obtenerConsultas();

  }

  obtenerConsultas(){
    this._consultasService.verConsultasRecepcion()
    .subscribe( data => {
      // console.log(data);
      this.setconsultas( data['data']);
      this.totalAmbulancia = data['data'].results;
    });
  }


 

  setconsultas( consultas ){
    this.consultas = consultas.reverse();
    console.log(this.consultas);
    
    this.setImp(this.consultas);
  }

  setImp(cons){    
    let i= 0;
    for (let element of cons) {  
      //console.log(element);
        
      this.imprimir.indice= i + 1;
      this.imprimir.fecha = element.fechaIngreso;
      this.imprimir.nombre = element.paciente.nombrePaciente;
      this.imprimir.genero = element.paciente.genero;
      this.imprimir.edad = element.paciente.edad;
      this.imprimir.medico = element.medicoTrante;
      //console.log(this.imprimir.indice);
      //console.log(this.imprimir);
      
      this.noSe.push(this.imprimir)
      this.imprimir = {
        indice: 0 ,
        fecha: '',
        nombre:  '',
        genero: '',
        edad: '',
        medico: '',
      }
      i++
    }    
  }

  

  imp(){
    console.log(this.noSe);
    
    let values: any;
    values = this.noSe.map((elemento) => Object.values(elemento));
    
    console.log(values);
    
    const doc:any = new jsPDF();

    doc.autoTable({
      head: [['#', 'Fecha', 'Nombre', 'Género', 'Edad', 'Médico Tratante']],
      body: values
    })

    doc.save('Historico_De_Pacientes.pdf')
  }
}
