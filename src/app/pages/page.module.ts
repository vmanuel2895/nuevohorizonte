import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { CommonModule } from '@angular/common';
import { RouterModule } from '@angular/router';
import { ChartsModule } from 'ng2-charts';
import { CdkStepperModule } from '@angular/cdk/stepper';
import { NgStepperModule } from 'angular-ng-stepper';


import { PacientesComponent } from './pacientes/pacientes/pacientes.component'
// import { ComponentsModule } from 'src/app/components/components.module';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { PageComponent } from './page/page.component';
import { ConsutaGralComponent } from './consultas/consuta-gral/consuta-gral.component';


import { PAGES_ROUTES } from './page.routes';
import { NgxPaginationModule } from 'ngx-pagination';
import { PdfViewerModule } from 'ng2-pdf-viewer';


import { HojaDiariaEnfGralComponent } from './enfermeria/hoja-diaria-enf-gral/hoja-diaria-enf-gral.component';
import { FichaEnfermeriaComponent } from './consultas/ficha-enfermeria/ficha-enfermeria.component';
import { HevolucionCGComponent } from './consultas/hevolucion-cg/hevolucion-cg.component';
import { RecetaComponent } from './consultas/receta/receta/receta.component';
import { ServiciosDashComponent } from './servicios/servicios-dash/servicios-dash.component';
import { ServiciosComponent } from './servicios/servicios/servicios.component';
import { FormularioComponent } from './servicios/formulario/formulario.component';
import { FormularioEditAmbComponent } from './servicios/formulario/formulario-edit-amb/formulario-edit-amb.component';
import { FormularioEditServiceComponent } from './servicios/formulario/formulario-edit-service/formulario-edit-service.component';
import { BitacoraConsultaGralComponent } from './consultas/doctores/bitacora-consulta-gral/bitacora-consulta-gral.component';
import { RegistroComponent } from './pacientes/registro/registro.component';
import { HojaEvolucionComponent } from './consultas/hojaEvolucion/hoja-evolucion/hoja-evolucion.component';
import { ConsultasComponent } from './consultas/bitacora/consultas/consultas.component';
import { FolioComponent } from './farmacia/folio/folio.component';
import { PagoComponent } from './servicios/pago/pago.component';
import { EstudiosPorAprobarComponent } from './consultas/estudios-por-aprobar/estudios-por-aprobar.component';
import { VerEstudiosComponent } from './consultas/ver-estudios/ver-estudios.component';
//  paquetes 
import { VerPaquetesComponent } from './paquetes/ver-paquetes/ver-paquetes.component';
import { PaqueteIDComponent } from '../pages/paquetes/paquete-id/paquete-id.component';
import { SolicitudComponent } from './paquetes/solicitud/solicitud.component';
import { VerPacienteComponent } from './pacientes/ver-paciente/ver-paciente.component';
import { PaquetePacienteComponent } from './pacientes/paquete-paciente/paquete-paciente.component';
import { ListadoMedicamentosComponent } from './farmacia/listado-medicamentos/listado-medicamentos.component';

//laboratorio
import { RegresosComponent } from './servicios/laboratorio/regresos/regresos.component';
import { CloroTinacoCisternaComponent } from './mantenimiento/cloro-tinaco-cisterna/cloro-tinaco-cisterna.component';
import { AnalisisBacteorologicoComponent } from './mantenimiento/analisis-bacteorologico/analisis-bacteorologico.component';
import { HojaServiciosComponent } from './servicios/laboratorio/hoja-servicios/hoja-servicios.component';
import { HojaReporteComponent } from './servicios/laboratorio/hoja-reporte/hoja-reporte.component';
import { NewStudioComponent } from './servicios/laboratorio/new-studio/new-studio.component';
import { GenerosPipe } from '../pipes/generos.pipe';
import { HistoricoEstudiosComponent } from './servicios/laboratorio/historico-estudios/historico-estudios.component';
// import { HistoricoEstudiosComponent } from './servicios/laboratorio/historico-estudios/historico-estudios.component';
import { ServicioComponent } from './admin/servicio/servicio.component';
import { ServicioPorUsuarioComponent } from './admin/servicio-por-usuario/servicio-por-usuario.component';
import { UtilidadServicioComponent } from './admin/utilidad-servicio/utilidad-servicio.component';
import { ServiciosPorClienteComponent } from './admin/servicios-clientes/servicios-por-cliente/servicios-por-cliente.component';
import { ExpedienteClinicoComponent } from './admin/expediente-clinico/expediente-clinico.component';
import { HojaConsumoAlDiaComponent } from './admin/hoja-consumo-al-dia/hoja-consumo-al-dia.component';
import { ComprasPorUtilidadComponent } from './admin/compras-por-utilidad/compras-por-utilidad.component';
import { AgregarServicioPorUtilidadComponent } from './admin/agregar-servicio-por-utilidad/agregar-servicio-por-utilidad.component';
import { ComponentsModule } from '../components/components.module';
import { BitacoraultraComponent } from './servicios/ultrasonido/bitacoraultra/bitacoraultra.component';
import { HojaUltrasonidoComponent } from './servicios/ultrasonido/hoja-ultrasonido/hoja-ultrasonido.component';
import { FormularioElementosComponent } from './servicios/ultrasonido/formulario-elementos/formulario-elementos.component';
import { HistoricoUltrasonidoComponent } from './servicios/ultrasonido/historico-ultrasonido/historico-ultrasonido.component';
import { DashboardResultadosComponent } from './servicios/recepcion-regresos/dashboard-resultados/dashboard-resultados.component';
import { EntregaDashComponent } from './recepcion/entrega-dash/entrega-dash.component';
import { RegresosLabComponent } from './servicios/laboratorio/regresos-lab/regresos-lab.component';
import { ResultadosFinalesComponent } from './servicios/recepcion-regresos/resultados-finales/resultados-finales.component';
import { SocioCanalComponent } from './admin/socio-canal/socio-canal.component';
import { DashComponent } from './admin/socio-canal/dashboard/dash/dash.component';
import { SedeComponent } from './admin/socio-canal/sede/sede.component';
import { FormularioCreadorComponent } from '../components/servicios/creador-servicios/formulario-creador/formulario-creador.component';
// import { BitacoraRayosXComponent } from './rayosX/bitacora/bitacora.component';
import { EntregaUltrasonidoComponent } from './servicios/recepcion-regresos/entrega-ultrasonido/entrega-ultrasonido.component';
import { ResultadosUltrasonidoComponent } from './servicios/recepcion-regresos/resultados-ultrasonido/resultados-ultrasonido.component';
import { ReporteUltrasonidoComponent } from './servicios/ultrasonido/reporteultra/reporte-ultrasonido/reporte-ultrasonido.component';
import { EditarServicioPorUtilidadComponent } from './admin/editar-servicio-por-utilidad/editar-servicio-por-utilidad.component';
import { BitacoraUltrasonidosComponent } from './servicios/recepcion-regresos/bitacora-ultrasonidos/bitacora-ultrasonidos.component';
import { BitacoraComponent } from './enfermeria/bitacora/bitacora.component';
import { ListadoDeSedesComponent } from './sedes/listado-de-sedes/listado-de-sedes.component';
import { DetailServiciosSedeComponent } from './sedes/detail-servicios-sede/detail-servicios-sede.component';
import { DashResultadosComponent } from './servicios/recepcion-regresos/dash-resultados/dash-resultados.component';
import { ServiciosRecepcionComponent } from './servicios/recepcion-regresos/servicios-recepcion/servicios-recepcion.component';
/////////////////////////////////// Rayos X ////////////////////////////////////////////

import { BitacoraRayosXComponent } from './servicios/rayos-x/bitacora-rayos-x/bitacora-rayos-x.component';
import { HojaRayosXComponent } from './servicios/rayos-x/bitacora-rayos-x/hoja-rayos-x/hoja-rayos-x.component';
import { HojaReporteRayosXComponent } from './servicios/rayos-x/bitacora-rayos-x/hoja-rayos-x/hoja-reporte-rayos-x/hoja-reporte-rayos-x.component';

import { HistoricoRayosXComponent } from './servicios/rayos-x/historico-rayos-x/historico-rayos-x.component';
import { FormularioRayosXComponent } from './servicios/rayos-x/historico-rayos-x/formulario-rayos-x/formulario-rayos-x.component';


////////////////////////////////////////////////////////////////////////////////////////
import { NgxDropzoneModule } from 'ngx-dropzone';
import { PedidosSedesSociosComponent } from './admin/pedidos-sedes-socios/pedidos-sedes-socios.component';
import { DetallePedidosSedesComponent } from './admin/detalle-pedidos-sedes/detalle-pedidos-sedes.component';
import { BitacoraRayosxComponent } from './servicios/recepcion-regresos/bitacora-rayosx/bitacora-rayosx.component';
import { EntregaRayosxComponent } from './servicios/recepcion-regresos/entrega-rayosx/entrega-rayosx.component';
import { ResultadosRayosxComponent } from './servicios/recepcion-regresos/resultados-rayosx/resultados-rayosx.component';
import { BitacoraHistoricoComponent } from './consultas/doctores/bitacora-historico/bitacora-historico.component';
import { ModalModule } from 'ng-modal-lib';
import { HojaEvolucionHistoricoComponent } from './consultas/hoja-evolucion-historico/hoja-evolucion-historico.component';




@NgModule({
  declarations: [
    GenerosPipe,
    PageComponent,
    PacientesComponent,
    ConsutaGralComponent,
    HojaDiariaEnfGralComponent,
    FichaEnfermeriaComponent,
    HevolucionCGComponent,
    RecetaComponent,
    ServiciosDashComponent,
    ServiciosComponent,
    FormularioComponent,
    FormularioEditAmbComponent,
    FormularioEditServiceComponent,
    //RecetaComponent,
    BitacoraConsultaGralComponent,
    RegistroComponent,
    HojaEvolucionComponent,
    ConsultasComponent,
    FolioComponent,
    PagoComponent,
    EstudiosPorAprobarComponent,

    ResultadosRayosxComponent,
    EntregaRayosxComponent,
    BitacoraHistoricoComponent,
    BitacoraRayosxComponent,

    VerEstudiosComponent,
    VerPaquetesComponent,
    PaqueteIDComponent,
    SolicitudComponent,
    VerPacienteComponent,
    PaquetePacienteComponent,
    ListadoMedicamentosComponent,
    RegresosComponent,
    CloroTinacoCisternaComponent,
    AnalisisBacteorologicoComponent,
    HojaServiciosComponent,
    HojaReporteComponent,
    NewStudioComponent,
    HistoricoEstudiosComponent,
    AnalisisBacteorologicoComponent,
    ServicioComponent,
    ServicioPorUsuarioComponent,
    ServiciosPorClienteComponent,
    ExpedienteClinicoComponent,
    HojaConsumoAlDiaComponent,
    ComprasPorUtilidadComponent,
    AgregarServicioPorUtilidadComponent,
    UtilidadServicioComponent,
    HistoricoUltrasonidoComponent,
    BitacoraultraComponent,
    HojaUltrasonidoComponent,
    FormularioElementosComponent,
    DashboardResultadosComponent,
    EntregaDashComponent,
    RegresosLabComponent,
    ResultadosFinalesComponent,
    SocioCanalComponent,
    DashComponent,
    SedeComponent,
    FormularioCreadorComponent,
    //  BitacoraRayosXComponent,
    EntregaUltrasonidoComponent,
    ResultadosUltrasonidoComponent,
    ReporteUltrasonidoComponent,
    EditarServicioPorUtilidadComponent,
    BitacoraUltrasonidosComponent,
    BitacoraComponent,
    ListadoDeSedesComponent,
    DetailServiciosSedeComponent,
    DashResultadosComponent,
    ServiciosRecepcionComponent,
    BitacoraRayosXComponent,
    HojaRayosXComponent,
    HojaReporteRayosXComponent,
    HistoricoRayosXComponent ,
    FormularioRayosXComponent,
    PedidosSedesSociosComponent,
    DetallePedidosSedesComponent,
    HojaEvolucionHistoricoComponent
    
    
  ],

  exports: [
    PageComponent,
    ReactiveFormsModule,
    CdkStepperModule,
    NgStepperModule,
  ],
  imports: [
    BrowserModule,
    ComponentsModule,
    FormsModule,
    RouterModule,
    ChartsModule,
    RouterModule,
    PAGES_ROUTES,
    ReactiveFormsModule,
    NgxPaginationModule,
    PdfViewerModule,
    CommonModule,
    NgxDropzoneModule,
    ModalModule
  ]


  
})







export class PageModule { }
