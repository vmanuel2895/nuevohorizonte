import { ComponentFixture, TestBed } from '@angular/core/testing';

import { PedidosSociosComponent } from './pedidos-socios.component';

describe('PedidosSociosComponent', () => {
  let component: PedidosSociosComponent;
  let fixture: ComponentFixture<PedidosSociosComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ PedidosSociosComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(PedidosSociosComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
