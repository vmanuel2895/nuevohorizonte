import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { PedidosSedesService } from 'src/app/services/pedidosSedes/pedidos-sedes.service';

@Component({
  selector: 'app-pedidos-sedes-socios',
  templateUrl: './pedidos-sedes-socios.component.html',
  styleUrls: ['./pedidos-sedes-socios.component.css']
})
export class PedidosSedesSociosComponent implements OnInit {

  public pedidos;
  public pagina = 0;
  public totalAmbulancia: string;;

  constructor( 
    private pedidosSedesService: PedidosSedesService,
    private _router: Router) { }

  ngOnInit(): void {
    this.obetenerPedidos();
    this.borrarLocal();
  }

  borrarLocal(){
    localStorage.removeItem('paciente')
    localStorage.removeItem('sedePaciente')
    localStorage.removeItem('carrito')
    localStorage.removeItem('idPedidoSede')
  }


  obetenerPedidos(){
    this.pedidosSedesService.obeterPedidos()
    .subscribe( data => {
        this.setPedidos( data['data'] )
    });
  }

  setPedidos(pedidos){
    this.pedidos = pedidos.reverse();
    this.totalAmbulancia = pedidos.data.results;
    /* console.log(  this.pedidos ); */
  }

  detalle(pedido,id){
    const est={
      status: 'Pagado'
    }
    if(pedido.status == 'Pagado'){
      /* console.log(pedido); */
      
      /* this.pedidosSedesService.actualizarEstado(id, est).subscribe((data:any)=>{
        if(data.ok){
          this.obetenerPedidos();
        }
      }) */
    }else{
      this._router.navigateByUrl('/pedidos/detalle/'+id);
    }
  }
}
