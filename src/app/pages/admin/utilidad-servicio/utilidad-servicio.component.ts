import { Component, OnDestroy, OnInit } from '@angular/core';
import { NgForm } from '@angular/forms';

import { utilidades } from '../../../interfaces/utilidades';

import { ServiciosService } from 'src/app/services/admin/servicios.service';
import { SedesService } from 'src/app/services/sedes/sedes.service';


import  Dates  from '../../../classes/dates/date.class';
import { Subscription } from 'rxjs';


@Component({
  selector: 'app-utilidad-servicio',
  templateUrl: './utilidad-servicio.component.html',
  styleUrls: ['./utilidad-servicio.component.css']
})
export class UtilidadServicioComponent implements OnInit, OnDestroy {


  public data:any[];
  public sedes:any[];
  public totales:number = 0;

  public charatecter$:Subscription;

  public busqueda = {
        sede:"TLYC01",
        fecha:""
      }

  public today ="";
  
  constructor(  
    private _servicios: ServiciosService,
    private _sedes: SedesService
     ) {
   }

  ngOnInit(): void {

    const date = new Dates();
    this.today = date.getDate();
    this.busqueda.fecha = this.today;

    this.getVentas();
    this.getSedesSelect();
  }

  getVentas(){
    

    this.charatecter$ =  this._servicios.getSells( this.busqueda )
    .subscribe( (data: utilidades) => {
        // console.log( data);
        this.setData(data['data'])
    });

  }


  
  setData( ventas ){
    // console.log( ventas);
    this.data = ventas;
    this.calcularTotales();
  }

  getSedesSelect(){
    this._sedes.getSedes()
    .subscribe( data => {this.setSedes( data['data'])});
    // console.log( )
  }

  setSedes( sedes ){
    // console.log( sedes )
    this.sedes = sedes;
  }

  setSedeSelect( sede: NgForm ){
    this.busqueda.sede = sede.value;
  }

  busquedaFecha(fecha: NgForm){
  
    this.busqueda.fecha = fecha.value;
    this.getVentas();

  }

  buscarPorFecha(){

    this._servicios.getSellsForDate( this.busqueda )
    .subscribe( data => {

      this.data = data['data'];
      this.calcularTotales();
    });
  }

  calcularTotales(){
    
    this.totales = 0;

    this.data.forEach( (elemento:utilidades) => {
      // console.log( elemento);
      this.totales = this.totales + elemento.totalCompra;
    });
  } 

  ngOnDestroy(){
    this.charatecter$.unsubscribe();
  }

}
