import { ComponentFixture, TestBed } from '@angular/core/testing';

import { ListadoDeSedesComponent } from './listado-de-sedes.component';

describe('ListadoDeSedesComponent', () => {
  let component: ListadoDeSedesComponent;
  let fixture: ComponentFixture<ListadoDeSedesComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ ListadoDeSedesComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(ListadoDeSedesComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
