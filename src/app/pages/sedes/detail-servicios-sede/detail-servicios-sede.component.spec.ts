import { ComponentFixture, TestBed } from '@angular/core/testing';

import { DetailServiciosSedeComponent } from './detail-servicios-sede.component';

describe('DetailServiciosSedeComponent', () => {
  let component: DetailServiciosSedeComponent;
  let fixture: ComponentFixture<DetailServiciosSedeComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ DetailServiciosSedeComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(DetailServiciosSedeComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
