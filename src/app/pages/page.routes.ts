import { Routes, RouterModule } from '@angular/router';
// components
import { PageComponent } from './page/page.component';
import { PacientesComponent } from './pacientes/pacientes/pacientes.component';
import { ConsutaGralComponent } from './consultas/consuta-gral/consuta-gral.component';
import { RegistroComponent } from './pacientes/registro/registro.component';
import { HojaDiariaEnfGralComponent } from './enfermeria/hoja-diaria-enf-gral/hoja-diaria-enf-gral.component';
import { FichaEnfermeriaComponent } from './consultas/ficha-enfermeria/ficha-enfermeria.component';
import { HevolucionCGComponent } from './consultas/hevolucion-cg/hevolucion-cg.component';
import { RecetaComponent } from './consultas/receta/receta/receta.component';
import { ServiciosDashComponent } from './servicios/servicios-dash/servicios-dash.component';
import { ServiciosComponent } from './servicios/servicios/servicios.component';
import { FormularioComponent } from './servicios/formulario/formulario.component';
import { FormularioEditAmbComponent } from './servicios/formulario/formulario-edit-amb/formulario-edit-amb.component';
import { FormularioEditServiceComponent } from './servicios/formulario/formulario-edit-service/formulario-edit-service.component';
import { ListadoMedicamentosComponent } from './farmacia/listado-medicamentos/listado-medicamentos.component'
import { ServicioComponent } from './admin/servicio/servicio.component';
import { UtilidadServicioComponent } from './admin/utilidad-servicio/utilidad-servicio.component';
import { SocioCanalComponent } from './admin/socio-canal/socio-canal.component';

//  LABORATORIO REGRESOS

import { RegresosComponent } from './servicios/laboratorio/regresos/regresos.component';
import { HojaServiciosComponent } from './servicios/laboratorio/hoja-servicios/hoja-servicios.component';
import { NewStudioComponent } from './servicios/laboratorio/new-studio/new-studio.component';
import { HojaReporteComponent } from './servicios/laboratorio/hoja-reporte/hoja-reporte.component';
import { HistoricoEstudiosComponent } from './servicios/laboratorio/historico-estudios/historico-estudios.component';

// ##################################  ULTRSONIDO REGRESOS  ###################################

import { BitacoraultraComponent } from './servicios/ultrasonido/bitacoraultra/bitacoraultra.component';
import { HojaUltrasonidoComponent } from './servicios/ultrasonido/hoja-ultrasonido/hoja-ultrasonido.component';
import { FormularioElementosComponent } from './servicios/ultrasonido/formulario-elementos/formulario-elementos.component';
import { HistoricoUltrasonidoComponent } from './servicios/ultrasonido/historico-ultrasonido/historico-ultrasonido.component';
import { ReporteUltrasonidoComponent } from './servicios/ultrasonido/reporteultra/reporte-ultrasonido/reporte-ultrasonido.component';

import { EntregaUltrasonidoComponent } from './servicios/recepcion-regresos/entrega-ultrasonido/entrega-ultrasonido.component';
import { ResultadosUltrasonidoComponent } from './servicios/recepcion-regresos/resultados-ultrasonido/resultados-ultrasonido.component';
import { BitacoraUltrasonidosComponent } from './servicios/recepcion-regresos/bitacora-ultrasonidos/bitacora-ultrasonidos.component';


//%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%% RECEPCION REGRESOS RESULTAODS DE ESTUDIOS %%%%%%%%%%%%%%%%%%%%%%%%%%%%5
import { DashboardResultadosComponent } from './servicios/recepcion-regresos/dashboard-resultados/dashboard-resultados.component';
import { ResultadosFinalesComponent } from './servicios/recepcion-regresos/resultados-finales/resultados-finales.component';
import { DashResultadosComponent } from './servicios/recepcion-regresos/dash-resultados/dash-resultados.component';
import { ServiciosRecepcionComponent } from './servicios/recepcion-regresos/servicios-recepcion/servicios-recepcion.component';

//############################################## RAYOS X######################################################
import { BitacoraRayosXComponent } from './servicios/rayos-x/bitacora-rayos-x/bitacora-rayos-x.component'
import { FormularioRayosXComponent } from './servicios/rayos-x/historico-rayos-x/formulario-rayos-x/formulario-rayos-x.component'
import { HistoricoRayosXComponent } from './servicios/rayos-x/historico-rayos-x/historico-rayos-x.component'
import { HojaRayosXComponent } from './servicios/rayos-x/bitacora-rayos-x/hoja-rayos-x/hoja-rayos-x.component'
import { HojaReporteRayosXComponent } from './servicios/rayos-x/bitacora-rayos-x/hoja-rayos-x/hoja-reporte-rayos-x/hoja-reporte-rayos-x.component'


import { BitacoraRayosxComponent } from './servicios/recepcion-regresos/bitacora-rayosx/bitacora-rayosx.component';
import { EntregaRayosxComponent } from './servicios/recepcion-regresos/entrega-rayosx/entrega-rayosx.component';
import { ResultadosRayosxComponent } from './servicios/recepcion-regresos/resultados-rayosx/resultados-rayosx.component';




// MANTENIMIENTO
import { CloroTinacoCisternaComponent } from './mantenimiento/cloro-tinaco-cisterna/cloro-tinaco-cisterna.component';
import { AnalisisBacteorologicoComponent } from './mantenimiento/analisis-bacteorologico/analisis-bacteorologico.component';



// GUARDS

import { RecpecionRolGuard } from '../gurads/recepcion/recpecion-rol.guard'
import { BitacoraConsultaGralComponent } from './consultas/doctores/bitacora-consulta-gral/bitacora-consulta-gral.component';
import { EnfermeriaGuard } from '../gurads/enfermeria/enfermeria.guard';
import { HojaEvolucionComponent } from './consultas/hojaEvolucion/hoja-evolucion/hoja-evolucion.component';
import { ConsultasComponent } from './consultas/bitacora/consultas/consultas.component';
import { FolioComponent } from './farmacia/folio/folio.component';


import { MedicosGuard } from '../gurads/medicos/medicos.guard'
import { FarmaciaRolGuard } from '../gurads/farmacia/farmacia-rol.guard';
import { PagoComponent } from './servicios/pago/pago.component';
import { VerPacienteComponent } from './pacientes/ver-paciente/ver-paciente.component';
import { EstudiosPorAprobarComponent } from './consultas/estudios-por-aprobar/estudios-por-aprobar.component';
import { VerEstudiosComponent } from './consultas/ver-estudios/ver-estudios.component';
import { VerPaquetesComponent } from './paquetes/ver-paquetes/ver-paquetes.component';
import { PaqueteIDComponent } from './paquetes/paquete-id/paquete-id.component';
import { SolicitudComponent } from './paquetes/solicitud/solicitud.component';
import { PaquetePacienteComponent } from './pacientes/paquete-paciente/paquete-paciente.component';
import { ServicioPorUsuarioComponent } from './admin/servicio-por-usuario/servicio-por-usuario.component';
import { ServiciosPorClienteComponent } from './admin/servicios-clientes/servicios-por-cliente/servicios-por-cliente.component';
import { AgregarServicioPorUtilidadComponent } from './admin/agregar-servicio-por-utilidad/agregar-servicio-por-utilidad.component';
import { ComprasPorUtilidadComponent } from './admin/compras-por-utilidad/compras-por-utilidad.component';
import { ExpedienteClinicoComponent } from './admin/expediente-clinico/expediente-clinico.component';
import { HojaConsumoAlDiaComponent } from './admin/hoja-consumo-al-dia/hoja-consumo-al-dia.component';
import { EntregaDashComponent } from './recepcion/entrega-dash/entrega-dash.component';
import { RegresosLabComponent } from './servicios/laboratorio/regresos-lab/regresos-lab.component';
import { DashComponent } from './admin/socio-canal/dashboard/dash/dash.component';
import { SedeComponent } from './admin/socio-canal/sede/sede.component';
import { FormularioCreadorComponent } from '../components/servicios/creador-servicios/formulario-creador/formulario-creador.component'
import { EditarServicioPorUtilidadComponent } from './admin/editar-servicio-por-utilidad/editar-servicio-por-utilidad.component';
import { BitacoraComponent } from './enfermeria/bitacora/bitacora.component';
import { DetailServiciosSedeComponent } from './sedes/detail-servicios-sede/detail-servicios-sede.component';
import { PedidosSociosComponent } from './admin/pedidosSocios/pedidos-socios/pedidos-socios.component';
import { ListadoDeSedesComponent } from './sedes/listado-de-sedes/listado-de-sedes.component';
import { PedidosSedesSociosComponent } from './admin/pedidos-sedes-socios/pedidos-sedes-socios.component';
import { DetallePedidosSedesComponent } from './admin/detalle-pedidos-sedes/detalle-pedidos-sedes.component';
import { BitacoraHistoricoComponent  } from './consultas/doctores/bitacora-historico/bitacora-historico.component';
import { PagosPaquetesComponent } from '../components/paquetes/pagos-paquetes/pagos-paquetes.component';
import { HojaEvolucionHistoricoComponent } from './consultas/hoja-evolucion-historico/hoja-evolucion-historico.component';

// , canActivate: [ValidarRecepcionGuard]
const pagesRoutes: Routes = [
  {
    path: '',
    component: PageComponent,
    canActivate: [],
    children: [

      // aca se iran agregando las rutas para el dashboard 
      // rutas para el proceso de consulta
      // pacientes
      { path: 'paciente', component: PacientesComponent },
      { path: 'paciente/:id', component: VerPacienteComponent },
      { path: 'registro/pacientes', component: RegistroComponent },
      // consultas
      { path: 'ver/estudios/pendientes/por/aprobar', component: EstudiosPorAprobarComponent, canActivate: [RecpecionRolGuard] },
      { path: 'consulta', component: ConsutaGralComponent, canActivate: [RecpecionRolGuard] },
      { path: 'consulta/:id', component: ConsutaGralComponent },
      { path: 'hoja/diaria/enfermeria', component: HojaDiariaEnfGralComponent, canActivate: [EnfermeriaGuard] },
      { path: 'ficha-enfermeria-01/:id', component: FichaEnfermeriaComponent },
      { path: 'receta/medica/:id', component: RecetaComponent },
      { path: 'consultas/general', component: ConsultasComponent },
      { path: 'estudios/pedidos/:id', component: VerEstudiosComponent },

      // esta es la hisotria clinica
      { path: 'historia/clinica/:id', component: HevolucionCGComponent, canActivate: [MedicosGuard] },
      { path: 'hoja/evolucion/:id', component: HojaEvolucionComponent, canActivate: [MedicosGuard] },
      { path: 'receta/medica/:id', component: RecetaComponent, canActivate: [MedicosGuard] },
      { path: 'bitacora/medicina/general', component: BitacoraConsultaGralComponent, canActivate: [MedicosGuard] },
      { path: 'historico/consulta/general', component: BitacoraHistoricoComponent },
      { path: 'historico/hoja/evolucion/:id', component: HojaEvolucionHistoricoComponent },
      // servicios 
      { path: 'serviciosInt', component: ServiciosDashComponent },
      { path: 'serviciosInt/:servicio', component: ServiciosComponent },
      // pago de los servicios 
      { path: 'pago/servicios', component: PagoComponent, canActivate: [RecpecionRolGuard] },

      // pago de los servicios con usuario registrado
      { path: 'pago/servicios/:id', component: PagoComponent, canActivate: [RecpecionRolGuard] },

      { path: 'formulario/:servicio', component: FormularioComponent },
      { path: 'formularioeditar/:id', component: FormularioEditAmbComponent },
      { path: 'formularioeditarservice/:id', component: FormularioEditServiceComponent },
      // RUTAS DE LAS PAGINAS QUE DONFORMAN  FARMACIA
      { path: 'agregar/medicamento', component: FolioComponent, canActivate: [FarmaciaRolGuard] },
      { path: 'farmacia/listado/medicamento', component: ListadoMedicamentosComponent },      // paquetes

      //RUTA DE LOS PAQUETES DE LOS USUARIOS
      { path: 'consultar/paquetes', component: VerPaquetesComponent },
      { path: 'paquete/:id', component: PaqueteIDComponent },
      { path: 'solicitar/paquete/:id', component: SolicitudComponent },
      { path: 'paquete-paciente/:id', component: PaquetePacienteComponent },
      { path: 'pago-paquete',component: PagosPaquetesComponent},

      // TERMINAN LAS RUTAS DE FARMACIA

      //BITACORA DE ENFERMERÍA 
      { path: 'enfermeria/bitacora', component: BitacoraComponent },

      //RUTA ADMON  
      { path: 'utilidad', component: ServicioComponent },
      { path: 'utilidades/servicio', component: UtilidadServicioComponent },
      { path: 'servicios/por/usuario', component: ServicioPorUsuarioComponent },
      { path: 'servicios/por/cliente', component: ServiciosPorClienteComponent },

      { path: 'agregar/servicios/por/utlidad', component: AgregarServicioPorUtilidadComponent },
      { path: 'compras/por/utilidad', component: ComprasPorUtilidadComponent },
      { path: 'expediente/clinico', component: ExpedienteClinicoComponent },
      { path: 'hoja/consumo/por/usuario/:id', component: HojaConsumoAlDiaComponent },
      { path: 'dash/:id/:servicio', component: SocioCanalComponent },
      { path: 'dash/:id', component: DashComponent },
      { path: 'sedes/servicio', component: SedeComponent },
      { path: 'editar/utilidad/:servicio/:id', component: EditarServicioPorUtilidadComponent },
      //LABORATRIO REGRESOS RUTAS

      { path: 'bitacora/laboratorios', component: RegresosComponent },

      // #################### ULTRASONIDO ##############

      { path: 'bitacora/ultrasonido', component: BitacoraultraComponent },
      { path: 'hoja-ultrasonido/:id', component: HojaUltrasonidoComponent },
      { path: 'registro/elementos/ultrasonido', component: FormularioElementosComponent },
      { path: 'historico/ultrasonido/estudios', component: HistoricoUltrasonidoComponent },

      { path: 'bitacora/recepcion/ultrasonidos', component: BitacoraUltrasonidosComponent },
      { path: 'lista/entrega/ultrasonidos/:id', component: EntregaUltrasonidoComponent },
      { path: 'resultados/finales/ultrasonido/:id', component: ResultadosUltrasonidoComponent },
      // %%%%%%%%%%%%%%%%%%%%%% recepcion regresos estudios %%%%%%%%%%%%%%%%%%%%%%%%%%%%%5
      { path: 'resultado-servicios/:id', component: DashboardResultadosComponent },
      { path: 'registro/elementos/ultrasonido/:id', component: FormularioElementosComponent },
      { path: 'historico/ultrasonido/estudios', component: HistoricoUltrasonidoComponent },
      { path: 'hoja/reporte/ultrasonido/:id', component: ReporteUltrasonidoComponent },
      // %%%%%%%%%%%%%%%%%%%%%% recepcion regresos estudios %%%%%%%%%%%%%%%%%%%%%%%%%%%%%5
      { path: 'resultado-servicios/:id', component: DashboardResultadosComponent },
      { path: 'hoja-servicios/:id', component: HojaServiciosComponent },
      { path: 'nuevo/estudio/laboratorio/:id', component: NewStudioComponent },
      { path: 'hoja-reporte/:id', component: HojaReporteComponent },
      { path: 'historico-estudio', component: HistoricoEstudiosComponent },
      { path: 'bitacora/laboratorios', component: RegresosComponent },
      { path: 'entrega/resultados', component: RegresosLabComponent },
      { path: 'final/obtenidos/:id', component: ResultadosFinalesComponent },

      //############################################## RAYOS X######################################################
      { path: 'bitacora/rayosX', component: BitacoraRayosXComponent },
      { path: 'hoja/rayos/x/:id', component: HojaRayosXComponent },
      { path: 'reporte/rayos/x/:id', component: HojaReporteRayosXComponent },

      { path: 'formulario/rayos/x/:id', component: FormularioRayosXComponent },
      { path: 'historico/rayos/x', component: HistoricoRayosXComponent },


      {path:'recepcion/bitacora/rayosx',component:BitacoraRayosXComponent},
      {path:'recepcion/lista/entrega/rayosx/:id',component: EntregaRayosxComponent},
      {path:'resultado/servicio/rayosx/:id',component:ResultadosRayosxComponent},

      //////Formulario creador de servicios
      { path: 'reporte/ultrasonido/todo/:id', component: FormularioCreadorComponent },
      // MANTENIMIENTO RUTAS

      // pedidos de otras sedes

      { path:'ver/pedidos/sedes', component: PedidosSociosComponent },

      // entrega de resultados
      { path:'entrega/resultados/dash', component: EntregaDashComponent },
      
      /// modulos card de resultados recepcion 
      {path:'dashboard/recepcion/resultados',component:DashResultadosComponent},
      {path:'dashboard/recepcion/resultados/:servicio',component:ServiciosRecepcionComponent},

      // sedes 

      { path: 'listado/sedes/', component: ListadoDeSedesComponent },
      { path: 'servicios/sedes/:id', component: DetailServiciosSedeComponent },
      { path: 'servicios/pedidos/otras/sedes', component: PedidosSedesSociosComponent  },
      {  path: 'pedidos/detalle/:id', component: DetallePedidosSedesComponent  },
      
      // entrega de resultados
      { path: 'entrega/resultados/dash', component: EntregaDashComponent },
      /// modulos card de resultados recepcion 
      { path: 'dashboard/recepcion/resultados', component: DashResultadosComponent },
      { path: 'dashboard/recepcion/resultados/:servicio', component: ServiciosRecepcionComponent },


      { path: '', redirectTo: '/dashboard', pathMatch: 'full' },
    ]

  }
];

export const PAGES_ROUTES = RouterModule.forChild(pagesRoutes);
